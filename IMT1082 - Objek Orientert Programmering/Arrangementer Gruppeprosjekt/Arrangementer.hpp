//  GRUPPE 38
//
//  Arrangementer.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Arrangementer_hpp
#define Arrangementer_hpp

#include <stdio.h>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include <cstring>
#include "ListTool2B.hpp"


#include <iostream>
using namespace std;

class Arrangementer{
private:
    List* arrangementList;
	int sisteArr;
    
	public:
    Arrangementer();					//lager sortert liste
    ~Arrangementer();
    
    void arrDisplay();					//Arrangement display funksjon
	void arrSlett();					//arrangement slett funksjon
	void arrNytt();						//arrangement ny funksjon
	void kjopArrangement();				//arrangement kjøp funksjon
	void handling();					//funksjon for valg av arrangement funksjon
	void lesFraFil(ifstream & inn);		//lesfrafil
	void skrivTilFil();					//skrivtilfil
    void skrivArrTilFil();				// Skriver til ARR_NR.DTA
    void lesArrFraFil(char*);           // Leser fra ARR_NR-DTA
};



#endif /* Arrangementer_hpp */
