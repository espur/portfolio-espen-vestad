//  GRUPPE 38
//
//  Funksjoner.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Funksjoner_hpp
#define Funksjoner_hpp
#include <iostream>
#include <fstream>
#include "Kunde.hpp"
#include "Arrangementer.hpp"
#include "Kunder.hpp"
#include "Steder.hpp"
#include "Const.hpp"
#include "ListTool2B.hpp"
using namespace std;
//globale variabler



char les();												//leser og  karakter, uppercase
int  les(const char output[], int min, int max);		//leser inn ett heltall mellom gitt min og max
void les(const char t[], char s[], int LEN);			//leser inn en ikke-blank tekst

//void les(string)
//void SkrivtilFil();
//void LesFraFil();
//bool sjekkNr(char num[STRLEN]);


void skrivMeny();										//skriver ut meny
void lesKunderFraFil();									//leskunderfrafil funksjon
void lesStederFraFil();									//lesstederfrafil funksjon
void lesArrangementerFraFil();							//lesarrangementfrafil funksjon
void kundeMeny();										//kundemeny funksjon
void stedMeny();										//stedmeny funksjon
void skrivTilFil();										//skrivtilfil funksjon

#endif /* Funksjoner_hpp */

