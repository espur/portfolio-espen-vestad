#ifndef Const_hpp
#define Const_hpp

#include <stdio.h>

//extern greie må være her, slik at vi bare inkludere const.h når vi trenger dem

//const int STRLEN = 100;
//const int NAVNLEN = 30;     // Max lengde paa ett navn
//const int MAXOPPS = 5;      // OPP not OPPS

const int STRLEN = 80;		//maxlengde for inputt
const int NAVNLEN = 30;		//maxlengde for navn
const int MAXOPP = 5;		//Max andtall oppsett pÂ sted
const int DATOLEN = 9;		// Max lengde paa dato
//const char* filNavnArr = "filnavn.dta"; //const peker til filnavn for ifstream og ofstream
const int MAXPRIS = 10000, MINPRIS = 0; //const for Maxpris og Minstepris
const int MAXAAR = 9999, MINAAR = 1000;	//const for Max år og minst år 
const int MAXTLF = 11111111, MINTLF = 99999999; //conster for telefon nr
const int MAXDATO = 20301231, MINDATO = 20000101; //conster for valg av dato
const int MAXBILETTER = 100;							//maxbiletter pr sone
const int MAXRADER = 9, MAXSETER = 9, MAXPLASSE = 81;	//max rader, max seter og max plasser


#endif /* Const_hpp */
