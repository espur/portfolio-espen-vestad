//  Gruppe 38
//
//  Sone.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright � 2019 xxxhamcho. All rights reserved.
//

#ifndef Sone_hpp
#define Sone_hpp

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <cstring>
#include "ListTool2B.hpp"
#include "Const.hpp"
#include "Funksjoner.hpp"
#include "Enum.h"

using namespace std;

class Sone : public TextElement {
private:
	char* soneNavn;						//navnet til sonen
	int antallBiletterTilSalgs;			//antall billetter til salgs i sonen
	int antallBiletterSolgt;			//antall billetter solgt i sonen
	int prisPerBilett;					//pris per bilett i sonen
    soneType sonetype;					//enum for sonetype
    
public:
	//Sone();
	void kjopt(int);					//teller opp antall solgt og ned antall til salgs
    Sone(char* nvn, soneType);			//sone konstruktor
    Sone(char* nvn, ifstream & inn);
    Sone(Sone* s);
	~Sone();							//sone destruktor
	void skrivTilFil(ofstream & ut);	//skrivtilfil
    void display();						//display funksjon
	bool operator== (soneType);			//bool for sjekking etter samme sonetype
    int hentTotalBill();				//total biletter til salgs i sonen
	int sendPris();						//sender bilettenes pris
    bool skjekkNavn(char*);				// return sonenavn
    bool hentSoneType();				//henter sonetype
    void lesSonesData(ifstream & inn);	//henter data om sonen
};


#endif /* Sone_hpp */
