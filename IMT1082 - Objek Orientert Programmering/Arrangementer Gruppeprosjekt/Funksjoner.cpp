#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
//  GRUPPE 38
//
//  Funksjoner.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#include "Funksjoner.hpp"
using namespace std;

extern Kunder kundebase;
extern Steder stedbase;
extern Arrangementer arrangementbase;

char les() {//  Leser og upcaser ett tegn:
	char ch;   cin >> ch;   cin.ignore();   return (toupper(ch));
}



int les(const char output[], const int min, const int max) {	//leser inn ett heltall mellom gitt min og max
	int tall;
	cout << '\t' << output << ": ";
	cin >> tall;
	while (cin.fail() || tall > max || tall < min) {		//sjekker gyldig input
		cin.clear();										//fjerner error flag
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "\tUgyldig!" << endl << '\t' << output
			<< " ( " << min << " - " << max << " ): ";
		cin >> tall;
	}
	cin.ignore(numeric_limits<streamsize>::max(), '\n');	//tilsvarende cin.ignore()
	return tall;											//returnerer innlest tall
}


void les(const char t[], char s[], const int LEN) {			//  Leser en ikke-blank tekst:
	do {
		cout << '\t' << t << ": ";	cin.getline(s, LEN);	//  Ledetekst og leser.
	} while (strlen(s) == 0);								//  Sjekker at tekstlengden er ulik 0.
}

void skrivMeny() {				//funksjon som skriver ut meny til brukeren
	cout << endl
		<< "FOLGENDE KOMMANDOER ER TILGJENGELIGE:" << endl << endl
		<< "Arrangement:" << endl
		<< "A  D\t-\t Display" << endl
		<< "A  N\t-\t Nytt" << endl
		<< "A  K\t-\t Kjøp billett(er)" << endl
		<< "A  R\t-\t Returnere / Levere tilbake bilett(er)" << endl
		<< "A  E\t-\t Endre" << endl
		<< "A  S\t-\t Slett" << endl << endl << endl

		<< "Kunde(r):" << endl
		<< "K  D\t-\t Display" << endl
		<< "K  N\t-\t Nytt" << endl
		<< "K  E\t-\t Endre" << endl
		<< "K  S\t-\t Slett" << endl << endl << endl

		<< "Oppsett:" << endl
		<< "O  D\t-\t Display" << endl
		<< "O  N\t-\t Nytt" << endl
		<< "O  E\t-\t Endre" << endl
		<< "O  S\t-\t Slett" << endl << endl << endl

		<< "Sted:" << endl
		<< "S  D\t-\t Display" << endl
		<< "S  N\t-\t Nytt" << endl
		<< "S  S\t-\t Slett" << endl << endl << endl

		<< "Q   \t-\t Quit / Avslutt" << endl << endl;
}

void lesKunderFraFil() {					//leskunderfafil funksjon
	ifstream kunderInn("KUNDER.DTA");

	if (kunderInn) {								//lesfrafil til kunder
		kundebase.lesFraFil(kunderInn);
	}
	else {
		cout << "Kunder.dta finnes ikke\n";
	}

}

void lesStederFraFil() {							//lesfrafil funksjon for steder
    ifstream stederInn("STEDER.DTA");
    
    if (stederInn) {
        stedbase.lesFraFil(stederInn);
    }
    else
        cout << "STEDER.DTA finnes ikke\n";
}

void lesArrangementerFraFil() {						//lesfrafil til arrangement
    ifstream arrangementerInn("ARRANGEMENT.DTA");
    
    if (arrangementerInn) {
        arrangementbase.lesFraFil(arrangementerInn);
    }
    
    else
        cout << "Finner ikke ARRANGEMENT.DTA" << endl;
}

void skrivTilFil() {								//skriv til fil 
    
    cout << "Skriver til filer.........\n";
	kundebase.skrivTilFil();
    stedbase.skrivTilFil();
    arrangementbase.skrivTilFil();
}

