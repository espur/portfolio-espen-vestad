//  GRUPPE 38
//
//  Kunde.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Kunde_hpp
#define Kunde_hpp

#include <stdio.h>
#include <iostream>
#include <cstring>
#include "ListTool2B.hpp"

using namespace std;


class Kunde : public NumElement {
private:
	char* navn;				//kunde navn
	char* gateAdresse;		//kundes adresse
	char* postSted;			//kundes poststed
	char* mail;				//kundes mail
	int postNr, tlfNr;		//kundes PostNr, telefon nr
	int kundeNr;			//kundes Id Nummer
public:

	Kunde(int nr);//leser inn Kunde

	~Kunde();
	Kunde(int nr, ifstream & inn);         // Need to implement les og Skriv til fil in funksjoner firs and that requirs DTA filene
	//void skrivTilFil(ofstream & ut);       //
	void display();								//kunde display funksjon
	void skrivTilFil(ofstream &ut);				//kunde SkrivtilFil funksjon
	int sendKunde();							//funksjon for sending av kundeNr					
	bool sjekkNavn(char nvn[]);					//bool for sammenligning av Navn

};










#endif /* Kunde_hpp */

