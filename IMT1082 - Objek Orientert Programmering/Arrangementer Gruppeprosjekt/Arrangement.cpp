//  GRUPPE 38
//
//  Arrangement.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright � 2019 xxxhamcho. All rights reserved.
//kake

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Arrangement.hpp"
#include "Sone.hpp"
#include "Vrimle.hpp"
#include "Funksjoner.hpp"
#include "Stoler.hpp"
#include "Enum.h"
#include <iostream>
#include <cstring>
#include <cctype>
using namespace std;

extern Steder stedbase;

//arrangment konstruktor 
Arrangement::Arrangement(char* nvn, char* snvn, int ant):TextElement(nvn) { 
	char buffer[STRLEN];
	arrNR = ant;                    // Fjernet ant + 1000, skapte problemer
    les("\nArtist", buffer, STRLEN);
	artist = new char[strlen(buffer) +1]; strcpy(artist, buffer);//leser artistnavn
	velgEnum();						//velger arrangement type

    dato   = les("Dato (AAAA:MM:DD)", MINDATO, MAXDATO); //leser dato og tidspunkt
	time   = les("Time ( 1 - 23 )", 1, 23);
	minutt = les("Minutt ( 0 - 59 ) ", 0, 59);

    spilleSted = new char[strlen(snvn) + 1];	//leser spillested
    arrNavn = new char[strlen(nvn) + 1];		//leser arrangemnet navn
    strcpy(spilleSted, snvn);					//fyller inlest data i riktig plass
    strcpy(arrNavn, nvn);

}

// LesFraFil Funksjonen
Arrangement::Arrangement(int arrNR, char nvn[], ifstream & inn): TextElement(nvn){
    
    this->arrNR = arrNR;  //oppdater nummer
    
    int type;			
    char temp[NAVNLEN];
    
    arrNavn = new char[strlen(nvn) + 1];
    strcpy(arrNavn, nvn);
    
    inn.getline(temp, NAVNLEN);
    spilleSted = new char[strlen(temp) + 1];
    strcpy(spilleSted, temp);
    
    inn.getline(temp, NAVNLEN);
    artist = new char[strlen(temp) + 1];
    strcpy(artist, temp);
    
    inn >> dato >> time >> minutt >> type;
    inn.ignore(2);
    switch(type) {
        case 0: Type = musikk;      break;
        case 1: Type = theater;     break;
        case 2: Type = show;        break;
        case 3: Type = kino;        break;
        case 4: Type = sport;       break;
        case 5: Type = familie;     break;
        case 6: Type = festival;    break;
    }
     
    
    
    
}


void Arrangement::display() {			//display funksjon for arrangementer
	cout << "arrangement nr:    " << arrNR << endl;
	cout << "arragmentets navn: " << text << endl;
	cout << "spillested:        " << spilleSted << endl;
	cout << "artist:            " << artist << endl;
	cout << "Dato og Tid:       " << dato << "  //" << time << ":" << minutt << endl;
	cout << "type number and name:  " << Type << endl; 
	skrivEnum();

}

bool Arrangement::sjekkTekst(char* nvn) {	//sammenligner s�keord i tekst
	if (strstr(text, nvn))
		return true;
	else 
		return false;
}
bool Arrangement::likNr(int nr) {
	if (arrNR == nr)
		return true;
	else
		return false;
}

bool Arrangement:: likSted(char* nvn) { //sammenligner sted
	if (!strcmp(spilleSted, nvn))
		return true;
	else
		return false;
}
bool Arrangement::likDato(int dat) { //sammenligner Datoer
	if (dato == dat)
		return true;
	else
		return false;
}
bool Arrangement::likArtist(char* art) { //sammenligner artistnavn
	if (!strcmp(artist, art))
		return true;
	else
		return false;
}

void Arrangement::velgEnum() {	//funksjon for � velge enum type
	int valg;
	cout << "Velg Arrangement type\n"
		<< "\t1 = Musikk\n"
		<< "\t2 = Theater\n"
		<< "\t3 = show\n"
		<< "\t4 = kino\n"
		<< "\t5 = sport\n"
		<< "\t6 = familie\n"
		<< "\t7 = festival\n";
	valg = les("velg type arrangement(1-7): ", 1, 7);
	switch (valg) //bruker valg til � sette enum
	{
	case 1: Type = musikk;		break;
	case 2: Type = theater;		break;
	case 3: Type = show;		break;
	case 4: Type = kino;		break;
	case 5: Type = sport;		break;
	case 6: Type = familie;		break;
	case 7: Type = festival;	break;
	}
}

void Arrangement::skrivEnum() {		//skriver ut arrangement type
	switch (Type) {
	case musikk:	cout << "Musikk\n";		break;
	case theater:	cout << "Theater\n";	break;
	case show:		cout << "Show\n";		break;
	case kino:		cout << "Kino\n";		break;
	case sport:		cout << "Sport\n";		break;
	case familie:	cout << "Familie\n";	break;
	case festival:	cout << "Festival\n";	break;
	}
}

bool Arrangement::enumType(int enu) {	//sammeligner arrangement type
	arrType tmp;
	switch (enu){
	case 1: tmp = musikk; break;
	case 2: tmp = theater; break;
	case 3: tmp = show; break;
	case 4: tmp = kino; break;
	case 5: tmp = sport; break;
	case 6: tmp = familie; break;
	case 7: tmp = festival; break;
	}
	if (tmp == Type)
		return true;
	else
		return false;
}

bool Arrangement::validName(char* arrNvn) { //sjekker om navn matcher
    return !(strcmp(arrNavn, arrNvn));
}

void Arrangement::seteHandling(int kundeNr){

    /*
    char soneNavn[STRLEN];
    
    
    //int choice = les("Stol eller vrimlesone? (0,1)", 0, 1);
    int antOpps = stedbase.returnOppsett(spilleSted);
   
    
    Sone* sone = nullptr; Stoler* stol; Vrimle* vriml;
    les("soneNavn: ", soneNavn, STRLEN);
    if(sone->skjekkNavn(soneNavn) == 1){ //sjekke om det finnes
    //Sone* sone->kopiOpp[antOpps];
        
        for (int i = 1; i <= antOpps; i++){
            sone = (Sone*)kopiOpp[antOpps]->remove(soneNavn);
            kopiOpp[antOpps]->add(sone);
            if (*sone == stoler) {
                stol = (Stoler*)kopiOpp[antOpps]->removeNo(i);
                stol->kjopPlass(antBillet, kundeNr);
                int rad = stol->returnereRad();
                int sete = stol->returnereSete();
                kopiOpp[antOpps]->add(stol);
            }
            else{
                vriml = (Vrimle*)kopiOpp[antOpps]->removeNo(i);
                vriml->kjopPlass(antBillet, kundeNr);
                kopiOpp[antOpps]->add(vriml);
            }
        
        }
    }
    else{
        cout << "sone eksisterer ikke! \n";
    }
     */
    
    cout << "Hvilken sete-rad vil du ha? \n";
    
    
    rad = les("rad:", 1, MAXRADER);
    sete = les("sete", 1, MAXSETER);
    antBillet = les("hvor mange billetter: ", 1, MAXBILETTER);
    
    kart(kundeNr, antBillet, rad, sete);

	
    cout << "(N)ybestilling / Q(uit): \n";
    char valg = les();
    
    switch (valg) {
        case 'N': seteHandling(kundeNr);
            break;
            
        case 'Q': break;
        default:
            cout << "(N)ybestilling / Q(uit): \n";
            break;
    }
}

void Arrangement::kart(int kundeNr, int antBillet, int rad, int sete){
	

	//Sone* hp; hp->sendPris;
    //skrivBiletterTilFil(kundeNr, pris, rad, sete);
    cout
    << "O.....Allerede solgt" << endl
    << "x ....Din Billet" << endl
    << "- ..... ledige plasser"<< endl;
    
    int solgBillet[MAXRADER][MAXSETER] = { {0, 0} };
    cout << " ";
    for(int i = 1; i <= MAXSETER; i++) cout << i ; //seat number
    cout << endl;
    
    for(int j = 1; j <= MAXRADER; j++){       //row number
        cout << j;
        
        for (int c = 1; c <= MAXSETER; c++){ // already taken seats
            
            if(solgBillet[j][c] !=0 ) cout << "o";
            if(j == rad && c == sete){ //chosen row and seat
                for(int m = 1; m <= antBillet; m++){
                    solgBillet[j][c++] = kundeNr; //tickets will be next to eachother
                    cout << "x" ; // newly reserved
                }
            }
            
            if(solgBillet[j][c] == 0) cout << "-"; // empty places
        }
        cout << endl;
        
        //hentTotalBilett();    in sone to count up the solgt tickets
        
    }
    
    
}
void Arrangement::lesArr(char* nvn, List* liste) {
	if (kopiOpp2 == nullptr)
		kopiOpp2 = liste;
	/*spilleSted = new char[strlen(nvn) + 1];
	strcpy(spilleSted, nvn);*/
}
// Skriver til fil
void Arrangement::skrivTilFil(ofstream & ut) {
    ut << arrNavn << endl << spilleSted << endl << artist << endl
    << dato << endl << time << endl << minutt << endl << Type << endl;
}

Arrangement :: ~Arrangement() {
    delete[] arrNavn;
    delete[] spilleSted;
    delete[] artist;
	for (int i = 1; i <= antSone; i++){
		delete kopiOpp[i];
	}
}

void Arrangement::skrivBiletterTilFil(int kundNr,  int rad, int sete) {
	Sone* sone;
	int p;
	for (int i = 1; i <= kopiOpp2->noOfElements(); i++) {
		sone = (Sone*)kopiOpp2->removeNo(i);
		p = sone->sendPris();
	}
	
	ofstream ut("BILETTER.DTA", ios::app);
	if (ut) {
		ut << "\n----------------------------------------------------------";
		ut << "\nKunde Nummer: " << kundNr;
		ut << "\n----------------------------------------------------------";
		ut << "\n Arrangement : " << arrNavn;
		if (rad == 0 && sete == 0) { ut << "\n Vrimle Sone"; }
		else{
		ut << "\n Stolsone";
		ut << "\n\t Plass: Rad: " << rad << "sete: " << sete; }
		//ut << "\n Pris: " << pris;
		ut << "\n__________________________________________________________";
		ut << "\n\n";
	}
	else
		cout << "finner ikke filen";

}

void Arrangement::info() {
    stedbase.hentOppsett(spilleSted);
    
}
 
int Arrangement::hentArrNr() {
    return arrNR;
}

void Arrangement::lesArrNrFraFil(ifstream & inn) {
    int antOpps;
    char buffer[STRLEN];
    inn >> arrNR;   inn.ignore();
    inn.getline(buffer, STRLEN);
    spilleSted = new char[strlen(buffer) +1];
    strcpy(spilleSted, buffer);
    
    inn.getline(buffer, STRLEN);
    arrNavn = new char[strlen(buffer) +1];
    strcpy(arrNavn, buffer);
    
    inn >> antOpps >> dato >> time >> minutt;       inn.ignore();
    
    inn.getline(buffer, STRLEN);
    artist = new char[strlen(buffer) +1 ];
    strcpy(artist, buffer);
    
    inn.getline(buffer, STRLEN);
    
    if(strcmp(buffer, "musikk"))    Type = musikk;
    if(strcmp(buffer, "theater"))   Type = theater;
    if(strcmp(buffer, "show"))      Type = show;
    if(strcmp(buffer, "kino"))      Type = kino;
    if(strcmp(buffer, "sport"))     Type = sport;
    if(strcmp(buffer, "familie"))   Type = familie;
    if(strcmp(buffer, "festival"))  Type = festival;
    
    for (int i = 1; i <= kopiOpp2->noOfElements(); i++) {
        Sone* sone;
        sone = (Sone*) kopiOpp2->removeNo(i);
        kopiOpp2->add(sone);
        
        if (sone->hentSoneType()) {
            Stoler* stoler = (Stoler*) kopiOpp2->removeNo(i);
            stoler->lesStolersData(inn);
            kopiOpp2->add(stoler);
        }
        else {
            Vrimle* vrimle = (Vrimle*) kopiOpp2->removeNo(i);
            vrimle->lesVrimlesData(inn);
            kopiOpp2->add(vrimle);
        }
    }
        
}

void Arrangement::skrivArrNrTilFil(ofstream & ut) {
    int antOpps = stedbase.returnOppsett(spilleSted); // henter ant oppsett
    ut << arrNR << endl                               // i et sted
       << spilleSted << endl
       << arrNavn << endl
       << antOpps << endl
       << dato << endl
       << time << endl
       << minutt << endl
       << artist << endl;
    
    switch(Type) {
        case musikk:    ut << "musikk" << endl;     break;
        case theater:   ut << "theater" << endl;    break;
        case show:      ut << "show" << endl;       break;
        case kino:      ut << "kino" << endl;       break;
        case sport:     ut << "sport" << endl;      break;
        case familie:   ut << "familie" << endl;    break;
        case festival:  ut << "festival" << endl;   break;
    }
    
    // Skriver ut alle soner i alle oppsettene i et sted
    for (int i = 1; i <= antOpps; i++) {
        stedbase.hentOppsettListe(spilleSted, i, ut);
    }
}
