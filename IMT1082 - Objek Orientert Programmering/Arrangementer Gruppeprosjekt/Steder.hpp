//  Gruppe 38
//
//  Steder.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Steder_hpp
#define Steder_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "ListTool2B.hpp"
#include "Sted.hpp"
#include "Const.hpp"
using namespace std;


//extern Sted sted;

class Steder {
private:
	List* stedListe;				//liste for steder
	//Steder* sted;
public:
	Steder();						//steder konstruktor			
	~Steder();
	void stedsHandling(char);		//funksjons meny for steder
	void stedDisplay();				//sted display funksjon
	void stedNytt();				//nytt sted funksjon
	void stedFjern();				//fjern sted funksjon
	void lesFraFil(ifstream & inn);	//lesfrafil funksjon
	void skrivTilFil();				//skrivtilfil funksjon
	int antOppsett(char*);			//en funksjon som returnerer antall oppsett
	List* kopiOpps(char*, int nr);	//funksjon som kopierer liste
	void oppsettHandling(char);		//fullfører funksjons valg til bruker 
	void oppsettNytt();				//funksjon for å sette opp nytt opsett
    bool erIListe(char[]);			//funksjon som sjekker om 
    bool displayElement(char[]);	//
    void oppsettDisplay();			//skriver ut oppsett
    void oppsettEndre();			//funksjon for endringer i oppsett
    void hentOppsett(char*);        //Hente aktuelt oppsett for et arr. for displaying
    int  returnOppsett(char*);		//senter antall oppsett
    void hentOppsettListe(char*, int, ofstream & ut);	//henter opsettets liste
    void hentOppsettListeForSone(char*, int index);		
};





#endif /* Steder_hpp */
