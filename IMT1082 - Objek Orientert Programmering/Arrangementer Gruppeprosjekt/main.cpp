//  Gruppe 38
//
//  main.cpp
//  main_project_c++

//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <fstream>
#include "Steder.hpp"
#include "Kunde.hpp"
#include "Arrangementer.hpp"
#include "Kunder.hpp"
#include "Funksjoner.hpp"

using namespace std;


Steder stedbase;				//globale variabler
Arrangementer arrangementbase;	
Kunder kundebase;



int main()
{


    lesKunderFraFil();
    lesStederFraFil();
    lesArrangementerFraFil();

    
    
    skrivMeny();	//skrivmeny til bruker

    cout << "\n\nKommando: ";
    char valg = les();
    skrivMeny();
    while (valg != 'Q') {
        switch (valg) { //bruker velger handling
            case 'S':   stedbase.stedsHandling(valg);	 break;
            case 'O':   stedbase.oppsettHandling(valg);  break;
            case 'A':   arrangementbase.handling();      break;
            case 'K':   kundebase.handling();            break;
            default:    skrivMeny();                     break;
        
        }
    cout << "\n\nKommando: ";
    valg = les(); //valget til brukeren leses
    }
    
    //stedbase.skrivTilFil();			//funksjoner somikke ble ferdig
    //arrangementbase.skrivTilFil();	//
    kundebase.skrivTilFil();
     
    
    cout << endl;
    
    return 0;
}
