//  Gruppe 38
//
//  Vrimle.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright � 2019 xxxhamcho. All rights reserved.
//

#ifndef Vrimle_hpp
#define Vrimle_hpp

#include <stdio.h>
#include <fstream>
#include "ListTool2B.hpp"
#include "Sone.hpp"
#include "Const.hpp"
using namespace std;

class Vrimle : public Sone {
private:
    int antTotalt;							//totalt antall plasser
    int* billett;							//peker til bilett
		
public:
    Vrimle(char* nvn, soneType type);		//konstruktor
    Vrimle(char* nvn, ifstream & inn);		
    void skrivTilFil(ofstream & ut);		//skrivtilfil funksjon
    void endreOppsett();					//funksjon for å endre oppsett
    void display();							//display funksjon
	void kjopPlass(int, int);				//funksjon for kjøp av plass
    Vrimle(Vrimle & v);
    void lesVrimlesData(ifstream & inn);    // For ARR_NR data
};


#endif /* Vrimle_hpp */
