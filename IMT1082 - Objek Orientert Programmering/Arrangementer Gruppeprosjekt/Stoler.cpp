//  Gruppe 38
//
//  Stoler.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Stoler.hpp"
#include "Funksjoner.hpp"

Stoler::Stoler(char* nvn, soneType type) : Sone(nvn, type) {
    rader = les("Rader: ", 1, MAXRADER);
    seter = les("Seter: ", 1, MAXSETER);
    
    billett = new int*[rader + 1];
    for(int i = 1; i <= rader; i++){
        billett[i] = new int[seter + 1];
    }
    for(int i = 1; i <= rader; i++){
        for(int j = 1; j <= seter; j++)
            billett[i][j] = 0;
    }

}

Stoler::Stoler(char* nvn, ifstream & inn) : Sone(nvn, inn){
    inn >> seter >> rader;

    inn.ignore();
}

void Stoler::display(){
    Sone::display();
    cout << "rader: " << rader << endl;
    cout << "seter: " << seter << endl;
    soneMatrise();
}

int radNr, seteNr;
void Stoler::kjopPlass(int bill, int knum) {
	
	for (int i = 0; i <= bill; i++){
		radNr = les("Ønsket rad: ", 1, rader);
		seteNr = les("Ønsket sete: ", 1, seter);
		/*if (radNr < 10)
			billett[radNr + 1][seteNr];
		else
		billett[radNr][seteNr];
         */
	}
    
    Sone::kjopt(bill);
}


Stoler::Stoler(Stoler & s) : Sone((Sone*) & s){		// copy constructor
    
    rader = s.rader;
    seter = s.seter;
    
    billett = new int*[rader + 1];
    for(int i = 1; i <= rader; i++){
        billett[i] = new int[seter + 1];
    }
    for(int i = 1; i <= rader; i++){
        for(int j = 1; j <= seter; j++)
            billett[i][j] = 0;
    }
}
void Stoler::soneMatrise(){
    cout << "  ";
    for(int i = 1; i<=seter; i++)cout << i; //sete number
    cout << "\n";
    for (int i = 1; i <= rader; i++){ //row number
        cout << i << ":";
        for (int j = 1; j <= seter; j++){
            if (billett[i][j] != 0) cout << "x"; //already taken
            cout << "-";
        }
        cout << "\n";
    }
}
int Stoler::returnereRad(){
    return radNr;
}

int Stoler::returnereSete(){
    return seteNr;
}
void Stoler::skrivTilFil(ofstream & ut) {
	Sone::skrivTilFil(ut);
	ut << seter << '\t' << rader << endl;
	// KUNDENR MANGLER
}

void Stoler::lesStolersData(ifstream & inn) {
    Sone::lesSonesData(inn);
    inn >> seter >> rader;      inn.ignore();
}
