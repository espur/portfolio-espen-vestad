//  Gruppe 38
//
//  Sone.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Sone.hpp"
extern Arrangementer arrangementBase;

//konstruktor for sone
Sone::Sone(char* nvn, soneType type) : TextElement(nvn) {
    
    prisPerBilett = les("pris per bilett: ", 1 , MAXPRIS );	//leser relevant informasjon
    antallBiletterTilSalgs = les("Totalt antall billetter", 1, MAXBILETTER);
    antallBiletterSolgt = 0;
    sonetype = type;
    
    soneNavn = new char[strlen(nvn) + 1];
    strcpy(soneNavn, nvn);
}

Sone::Sone(char* nvn, ifstream & inn) : TextElement(nvn) {
    inn >> prisPerBilett >> antallBiletterTilSalgs;
}

void Sone::kjopt(int bill) { //funsjon holder styr på antall biletter soglt og til salgs
	antallBiletterTilSalgs -= bill;
	antallBiletterSolgt += bill;
}

Sone::~Sone() {	//desturctor
	delete[] soneNavn;
}
int Sone:: sendPris() {		//funksjon for sending av pris
	return prisPerBilett;
}
void Sone::skrivTilFil(ofstream & ut) {		//skrivtilfil funksjon

    char type[STRLEN];
    
    if (sonetype == stoler) //skriver stoler eller vrimle
        strcpy(type, "stoler");
    else
        strcpy(type, "vrimle");
    
    ut << soneNavn << endl << type << endl << prisPerBilett << '\t'
    << antallBiletterTilSalgs << '\t' << antallBiletterSolgt << endl;
    
}

void Sone::display() {		//display funksjon for sone
    char type[STRLEN];
    
    if (sonetype == stoler)
        strcpy(type, "(STOLSONE)");
    else
        strcpy(type, "(VRIMLESONE)");
    
    cout << "\n" << type << endl
    << "Sonenavn: " << soneNavn << endl
    << "Antall til salgs: " << antallBiletterTilSalgs << endl
    << "Antall solgt: " << antallBiletterSolgt << endl
    << "Pris: " << prisPerBilett << endl;
    
    
}

bool Sone ::operator== (soneType typ) {	//sjekker om sonetype er lik
	if (sonetype == typ)
		return 1;
	else
		return 0;
}

int Sone::hentTotalBill() {		//henter antall biletter soglt for en sone
    return antallBiletterTilSalgs;
}

Sone::Sone(Sone* s) : TextElement(s -> text){
    sonetype = s -> sonetype;
    prisPerBilett = s -> prisPerBilett;
    antallBiletterSolgt = s -> antallBiletterSolgt;
    antallBiletterTilSalgs = s -> antallBiletterTilSalgs;
}
bool Sone::skjekkNavn(char* soneNvn){	//sammenligner sonenavn
    return !(strcmp(soneNavn, soneNvn));
}



bool Sone::hentSoneType() { //sjekker sonetypen for sonen
    if (sonetype == stoler)
        return 1;
    else
        return 0;
}

void Sone::lesSonesData(ifstream & inn) { //leser sonens data
    char buffer[STRLEN];
    
    inn.getline(buffer, STRLEN);	//henter sonenavn
    soneNavn = new char[strlen(buffer) + 1];
    strcpy(soneNavn, buffer);
    
    inn.getline(buffer, STRLEN);	//sjekker sonetype
    if (strcmp(buffer, "vrimle"))   sonetype = vrimle;
    if (strcmp(buffer, "stoler"))   sonetype = stoler;
    //henter informasjon
    inn >> prisPerBilett >> antallBiletterTilSalgs >> antallBiletterSolgt;
}
