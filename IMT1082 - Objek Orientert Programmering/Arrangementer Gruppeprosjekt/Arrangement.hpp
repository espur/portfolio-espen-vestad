//  GRUPPE 38
//
//  Arrangement.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Arrangement_hpp
#define Arrangement_hpp
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "Sted.hpp"
#include "Steder.hpp"
#include "Const.hpp"
#include "cstring"
#include "Enum.h"
#include "ListTool2B.hpp"

class Arrangement :public TextElement {
private:

	int arrNR;							//et unikt arrangement NR
	int antSone;						//antall soner 
	int dato, time, minutt;				//int'er for Datoe, og Tidspunk
    int rad, sete, antBillet;			//int for rad, sete og antall biletter
	char* arrNavn;						//sorteres på navn, men navnet trengs ikek være unikt
	char* spilleSted;					//navnet på spillestedet
	char* artist;						//navnet på artisten
	arrType Type;						//Enum for valg av arrangement type
	List* kopiOpp[MAXOPP];				//en liste for kopiering av opsettet
    int solgBillet[MAXRADER][MAXSETER];	//brukes til å sjekek om en spesefi plass er solgt
    List* kopiOpp2 = nullptr;			//en liste for kopiering av opsettet


	// lage pointer for oppsett Oppsett *arrOppsett
public:

	Arrangement(char*, char*, int);						//konstruktor for arrangement
    Arrangement(int arrNR, char nvn[], ifstream & inn); //lesfrafil Funksjon
    Arrangement(char* nvn, ifstream & inn);				//skrivtilfil Funksjon
	~Arrangement();										//Destructor
	
	void kjop(int knum);			//en funksjon for å kjøpe bilett(er) til et arrangement
	void lesArr(char*, List*);		//funksjon for å kopiere et arrangement
	void skrivEnum();				//skriver ut enumen til arrangementet
	void display();					//display funksjon for arangementets informasjon
	void velgEnum();				//en funksjon for å velge arrangementets enum
    void info();					//Displayer info om soner osv i arrangementer
	void seteHandling(int kundeNr); //en funksjon for håndetring av sete bestilling
	void kart(int kundeNr, int antBillet, int rad, int sete); //en funksjon for håndering og utskrift av sete Kart
	void skrivTilFil(ofstream & ut);			// skrivtilfil funksjon	
	void skrivBiletterTilFil(int ,int,int);	//skriv bilettertilfil funksjon
	void skrivArrNrTilFil(ofstream & ut);

	bool sjekkTekst(char* t);		//sammen ligner medsent tekst med arrangement navn
    //bool likNavn(char* arrNavn);
	bool likNr(int);				//sjekker at arrangement Nr Matcher
	bool likSted(char*);			//sjekekr at arrangement Sted matcher
	bool likDato(int dato);			//sjekker at arrangement dato matcher
	bool enumType(int enu);			//sjekker om Enum type matcher
	bool likArtist(char*);			//sjekker om Artist Matcher
	bool validName(char*);			//sjekekr at medsendt navn og Arrangement navn er like
    int hentArrNr();				//en funksjon som sender arrangement Nr
    void lesArrNrFraFil(ifstream & inn);
};

#endif /* Arrangement_hpp */

