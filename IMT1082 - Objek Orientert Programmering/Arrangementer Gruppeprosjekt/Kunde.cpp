//  GRUPPE 38
//
//  Kunde.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif


#include <iostream>
#include <fstream>
#include <cstring>
#include "Const.hpp"
#include "Kunde.hpp"
#include "Funksjoner.hpp"


Kunde::Kunde(int nr) : NumElement(nr) {
	this->kundeNr = nr;

	cout << "Kunde Nummer(" << kundeNr << ") : " << endl;

	char temp[STRLEN];

	les("Kundens navn", temp, NAVNLEN);
	navn = new char[strlen(temp) + 1];    // lage en new char array
	strcpy(navn, temp);                   // kopiere til

	les("Gateadresse", temp, STRLEN);     // lese inn
	gateAdresse = new char[strlen(temp) + 1];
	strcpy(gateAdresse, temp);

	les("Poststed", temp, STRLEN);        // lese inn
	postSted = new char[strlen(temp) + 1];
	strcpy(postSted, temp);

	postNr = les("Postnummer", 1, 9999);

	tlfNr = les("Mobilnummer", MAXTLF, MINTLF);

	les("Mailadresse", temp, STRLEN);     // lese inn
	mail = new char[strlen(temp) + 1];
	strcpy(mail, temp);

}

Kunde::Kunde(int nr, ifstream & inn) : NumElement(nr) {

	this->kundeNr = nr;

	char temp[STRLEN];

	inn >> tlfNr; inn.ignore();
	inn >> postNr; inn.ignore();

	inn.getline(temp, STRLEN);
	navn = new char[strlen(temp) + 1];    // lage en new char array
	strcpy(navn, temp);                   // kopiere over

	inn.getline(temp, STRLEN);
	gateAdresse = new char[strlen(temp) + 1];    // lage en new char array
	strcpy(gateAdresse, temp);                   // kopiere over

	inn.getline(temp, STRLEN);
	postSted = new char[strlen(temp) + 1];    // lage en new char array
	strcpy(postSted, temp);                   // kopiere over

	inn.getline(temp, STRLEN);
	mail = new char[strlen(temp) + 1];    // lage en new char array
	strcpy(mail, temp);                   // kopiere over

}

Kunde::~Kunde() {                  // Dekonstruktor
	delete[] navn;            // Sletter navn og frigjor memory
	delete[] gateAdresse;         // Sletter gataadresse
	delete[] postSted;
	delete[] mail;
}


void Kunde::display() {		//display funksjon skriver informasjon om kunde
	cout
		<< "kunde nr :" << kundeNr << endl
		<< "navn :" << navn << endl
		<< "addresse :" << gateAdresse << endl
		<< "poststed: " << postSted << endl
		<< "mail: " << mail << endl
		<< "postnr: " << postNr << endl
		<< "tlf: " << tlfNr << endl << endl;
}

void Kunde::skrivTilFil(ofstream &ut) {			//skriver spiller til fil
	ut  << kundeNr << endl
		<< tlfNr << endl
		<< postNr << endl
        << navn << endl
		<< gateAdresse << endl
		<< postSted << endl
		<< mail << endl <<endl;
    
}

bool Kunde::sjekkNavn(char nvn[]) {		//bool funksjon for sammenligning av Navn
	return strcmp(navn, nvn);
}
int Kunde::sendKunde() {				//funksjon som sender Kunde Nr
	return kundeNr;
}
