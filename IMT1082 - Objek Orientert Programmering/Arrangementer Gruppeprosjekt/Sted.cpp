//  Gruppe 38
//
//  Sted.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Sted.hpp"
#include "Sone.hpp"
#include "Vrimle.hpp"
#include "Stoler.hpp"

extern Steder stedbase;

//sted konstruktor
Sted::Sted(char* navn) : TextElement(navn) {
    antallOpps = 0;		//setter antall opsett og soner til 0
	antallSoner = 0;
	
    stedNavn = new char[strlen(navn) + 1]; //leser og setter stednavn
	strcpy(stedNavn, navn);
}

Sted::~Sted() { //sted destructor
	delete[] stedNavn;
}

Sted::Sted(char* nvn, ifstream & inn) : TextElement(nvn) {
    for (int i = 1; i <= MAXOPP; i++) { //går igjenom alle opsett
        oppsett[i] = new List(Sorted); //lager liste 
    }
    
    stedNavn = new char[strlen(nvn) +1 ]; //leser navn
    strcpy(stedNavn, nvn);
    
    int antallSoner = 0;
    char soneNavn[STRLEN];
    char soneType[STRLEN];
    
    inn >> antallOpps; inn.ignore();
    
    for (int i = 1; i <= antallOpps; i++) { //går igjenom alle opsett
        inn >> antallSoner; inn.ignore();
        
        for (int c = 1; c <= antallSoner ; c++) { //går igjenom alle soner
            inn.getline(soneNavn, STRLEN);
            inn.getline(soneType, STRLEN);
            
            if (!strcmp(soneType, "stoler")) {		//skriver sone type
                Stoler* stoler = new Stoler(soneNavn, inn);
                oppsett[i]->add(stoler);
            }
            if (!strcmp(soneType, "vrimle")) {
                Vrimle* vrimle = new Vrimle(soneNavn, inn);
                oppsett[i]->add(vrimle);
            }
            
        }
        
    }
}

		//skriv til fil funksjon
void Sted::skrivTilFil(ofstream & ut) {
	ut << text << endl << antallOpps << endl;
    for(int i = 0; i <= antallOpps; i++){ //går igjenom opsettene og skriver sonene
        int buffer = oppsett[i] -> noOfElements();
        ut << buffer << endl;       //antall sone
        for(int j = 0; j <= buffer; j++){
            Sone* sone = (Sone*)oppsett[i] -> removeNo(j);	
            //sone -> skrivTilFil(utfil, 1);
            oppsett[i] -> add(sone);
        }
    }
    
}


void Sted::display() { //display funksjon for Sted
	cout << "Stedsnavn: " << text << endl
		 << "Antall oppsett på dette stedet: "
		 << antallOpps << endl;
}


void Sted::oppsettDisplay() { //dislay funksjon for opsett
    int nr = les("Oppsett nr ", 1, antallOpps);
    if(!oppsett[nr]->isEmpty())
    oppsett[nr]->displayList();
    cout << "ikke noe oppsett på dette stedet! \n";
}



void Sted::nyttOppsett() { //ny opsett funksjon
    if (antallOpps < MAXOPP) {
        cout << "Oppsett er (T)otalt nytt eller (K)opiert og endret po ?\n";
        char valg = les();
        while(valg != 'Q' ){
            switch (valg) { //lager nytt opsett om det er plass og gir to valg
                case 'T':   nyttOppsettScratch();   break;
                
                case 'K':   kopiertOppsett();   break;
                default:
                    cout << "ugyldig kommando\n";
                    break;
                }
            cout << "Oppsett er (T)otalt nytt eller (K)opiert og endret po ? .. (Q)uit\n";
            valg = les();

        }
        
    }
    else{
        cout << "max 5 oppsett pa stedet!! full \n";
    }
    

}


void Sted::kopiertOppsett(){ //kopierer et opsett til et nytt
    
    int buffer = les("Oppsett nr som du vil kopiere: ", 1, MAXOPP);
    
    if (!oppsett[buffer]->isEmpty()) {
        if (antallOpps < MAXOPP) {
            oppsett[++antallOpps] = copycat(buffer);
        }
    }
    /*int buffer = les("Oppsett nr som du vil kopiere: ", 1, MAXOPP);
    //buffer--;
    
    if (!oppsett[buffer]->isEmpty()) {
        if (antallOpps < MAXOPP) {
            for (int i = 0; i <= oppsett[antallOpps]->noOfElements(); i++) {
                Element* element = oppsett[antallOpps]->remove(i);
                
                Element* element1 = element;
                oppsett[antallOpps+1]->add(element1);
                
                oppsett[antallOpps]->add(element);
          
            }
        }
    }
    antallOpps++;
     */
}


void Sted::nyttOppsettScratch() { //lager et helt nytt oppsett
    soneType temp;
    char buffer[STRLEN];
    
    oppsett[++antallOpps] = new List(Sorted); //lager ny liste for opsettet
    
    cout << "Oppsett nr. " << antallOpps << "\n";
    les("Sonenavn (avslutt med 'Q'))", buffer, STRLEN);
    
    while (!oppsett[antallOpps]-> inList(buffer) &&  buffer[0] != 'Q'){
        
        
        cout << "Sonetype:(S(toler) eller V(rimle): \n";
        
        switch (les()) { //leser g velger info til oppsettet
            case 'S': {
                temp = stoler;
                Stoler* stoler = new Stoler(buffer, temp);
                oppsett[antallOpps]->add(stoler);  break;
            }
            case 'V':{
                temp = vrimle;
                Vrimle* vrimle = new Vrimle(buffer, temp);
                oppsett[antallOpps]->add(vrimle);  break;
            }
        
            default:
                cout << "Ugyldig kommando\n\n";  break;
    
        }
        
        les("Sonenavn (avslutt med 'Q'))", buffer, STRLEN);
    }

    //cout << "\n\nKommando: ";
    //valg = les();
}

bool Sted::sjekkStedsNavn(char nvn[]) { //sammenligner stedsnavn
    
    return strcpy(stedNavn, nvn);
}

void Sted::endreOppsett() {				//funksjon for endring på opsett
    char valg;
    int nr;
    
    nr = les("Velg oppsett nr å endre", 1, antallOpps); //velge opsett Nr
    
    if(antallOpps > 0) {
        cout << "Legge til, fjerne eller endre på Sone? (L, F, E): ";
        valg = les();       cout << endl;		//valg for endre sone
        
        switch(valg) {
            case 'L':   leggTilSone(nr);    break;
            case 'F':   fjernSone(nr);      break;
            case 'E':   endreSone(nr);      break;
            case 'Q':                       break;
            default: cout << "Ugyldig kommando\n";  break;
        }
    }
    
    else
        cout << "Det finnes ingen oppsett\n";
}


void Sted::endreSone(int oppsNr) {                // Endre en medsendt SONE
    Vrimle* tempVrimle = nullptr; //lager hjelpepekere og buffer
    Stoler* tempStoler = nullptr;
    char buffer[STRLEN];
    soneType temp;
    
    les("Modifisere hvilken sone(sone navn)?", buffer, STRLEN); //velg sone
    
	//henter informasjon fra soner
    tempVrimle = (Vrimle*) oppsett[oppsNr]->remove(buffer);
    tempStoler = (Stoler*) oppsett[oppsNr]->remove(buffer);
    
    if (tempVrimle != nullptr) { //fyller til valgt kategori
        temp = vrimle;
        oppsett[oppsNr]->destroy(buffer);
        tempVrimle = new Vrimle(buffer, temp);
        oppsett[oppsNr]->add(tempVrimle);
    }
    else if (tempStoler != nullptr) {
        temp = stoler;
        oppsett[oppsNr]->destroy(buffer);
        tempStoler = new Stoler(buffer, stoler);
        oppsett[oppsNr]->add(tempStoler);
    }
    else
        cout << "Sonen er ikke registrert\n";
}

List* Sted::copycat(int nr) { //funksjon for kopiering av oppsett
    
    List* liste = nullptr; //hjelpepekere og inter
    int i, ant;
    Sone *sone, *kopi;
    
    if (nr >= 1 && nr <= antallOpps) { //hvis det er gyldig menge oppsett
        ant = oppsett[nr]->noOfElements(); //henter antall oppsett
        
        liste = new List(Sorted);			//lager ny liste
        for (i = 1; i <= ant; i++) {
            sone = (Sone*) oppsett[nr]->removeNo(i);	//henter og fyller inn oppsettet
            if (*sone == stoler) kopi = new Stoler(*((Stoler*) sone));
            else kopi = new Vrimle(*((Vrimle*) sone));
            
            oppsett[nr]->add(sone);
            liste->add(kopi); //Legger ny kopi i liste
        }
    }
    
    return liste; //returner liste
}

void Sted::leggTilSone(int oppsNr) { //leggtil sone funksjon
    Vrimle* tempVrimle = nullptr; //hjelpepekere og buffere
    Stoler* tempStoler = nullptr;
    soneType temp;
    char buffer[STRLEN];
    char valg;
    
    cout << "Vrimle (V) eller Stol (S) sone? Q(Quit)";
    valg = les();       cout << endl;
    
    while(valg != 'Q') { //velge type sone
        les("Navn på ny sone", buffer, STRLEN);
        
        switch(valg) {
            case 'V': {
                temp = vrimle; //lager vrimle sone
                tempVrimle = new Vrimle(buffer, temp);
                oppsett[oppsNr]->add(tempVrimle);
            } break;
            
            case 'S': {
                temp = stoler; //lager stol sone
                tempStoler = new Stoler(buffer, temp);
                oppsett[oppsNr]->add(tempStoler);
            } break;
            
            default:  cout << "Ugyldig kommando\n";         break;
        }
        cout << "Vrimle (V) eller Stol (S) sone? Q(Quit)";
        valg = les();       cout << endl;
    }
}

void Sted::fjernSone(int oppsNr) { //fjerner medsendt sone
    char buffer[STRLEN];
    
    les("Hvilken sone vil du fjerne?", buffer, STRLEN);
    
    if(oppsett[oppsNr]->inList(buffer)) { //henter ut sone fra liste og fjerner den
        oppsett[oppsNr]->destroy(buffer);
        cout << "Sone '" << buffer << "' er fjernet." << endl;
    }
}

int Sted::antOpps() { //returnerer antall oppsett
    return antallOpps;
}

List* Sted::returnListe(char* stednvn, int index) {
    if(oppsett[index]->noOfElements() != 0) //returnere antal soner i opsettet
        return oppsett[index]; //returner listen med soner i oppsettet
    else
        return nullptr;
}
