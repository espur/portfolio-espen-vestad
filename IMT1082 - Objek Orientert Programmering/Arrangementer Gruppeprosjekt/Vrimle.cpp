//  Gruppe 38
//
//  Vrimle.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Vrimle.hpp"
#include "Sone.hpp"
#include "Const.hpp"

Vrimle::Vrimle(char* nvn, soneType type) : Sone(nvn, type) {
    
}

Vrimle::Vrimle(char* nvn, ifstream & inn): Sone(nvn, inn){
    inn.ignore();
}

void Vrimle::display(){
    Sone::display();                            // Displaye arvet materiell
    
}

void Vrimle::kjopPlass(int bill, int knum) {    // Kjøpe plas i sone
    
	bool plass = false;
    int antallBill = hentTotalBill();
	
    for (int i = 1; i <= bill; i++) {
		
		for (int i = 1; i <= antallBill; i++){
			if ((billett[i] = 0)) {
				billett[i] = knum;
				plass = true;
			}
			else
				plass = false;
		}
	}
	if (!plass)
		cout << "\n ingen ledige plasser!\n";
    
    Sone::kjopt(bill);
}

void Vrimle::skrivTilFil(ofstream & ut) {
	Sone::skrivTilFil(ut);
}


Vrimle::Vrimle(Vrimle & v) : Sone((Sone*) & v){			// copy constructor
    antTotalt = v.antTotalt;
    
    billett = new int[antTotalt + 1];
    for(int i = 1; i <= antTotalt; i++) billett[i] = 0;
}


void Vrimle::lesVrimlesData(ifstream & inn) {       // Ifm arrData
    Sone::lesSonesData(inn);
}

