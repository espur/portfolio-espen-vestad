//  Gruppe 38
//
//  Sted.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Sted_hpp
#define Sted_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "ListTool2B.hpp"
#include "Const.hpp"
#include "Funksjoner.hpp"
#include "Sone.hpp"
using namespace std;

class Sted : public TextElement {
private:
	int antallOpps;					//antall oppsett på sted
	int antallSoner;				//antall soner i oppsett
	List* oppsett[MAXOPP];			//liste for opsett
    List* soneListe;				//liste for soner
	char* stedNavn;					//stedsnavn

public:
	Sted(char* navn);					//konstruktor
	Sted(char* nvn, ifstream & inn);	
	~Sted();							
	void skrivTilFil(ofstream & ut);	//skrivtilfil funksjon
	void display();						//display fuksjon
	bool sjekkStedsNavn(char nvn[]);	//sammenligner 
    void nyttOppsett();					//funksjon for å sette opp nytt oppsett
    void oppsettDisplay();				//display opsett funksjon
    void nyttOppsettScratch();			//funksjon for å lage helt nytt opsett
	List* copycat(int);
    void endreOppsett();				// Endre et eksisterende oppsett O E
    void endreSone(int nr);             // Endre en sone i et gitt oppsett
    void leggTilSone(int nr);           // Legge til en ny sone i gitt oppsett
    void fjernSone(int nr);             // Fjerne en sone fra et gitt oppsett
	int  antOpps();						// sender antall opsett
    void kopiertOppsett();				// kopierer opsett
    List* returnListe(char*, int);		// 
   
    
};


#endif /* Sted_hpp */
