//  GRUPPE 38
//
//  Kunder.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Kunder_hpp
#define Kunder_hpp
#include <stdio.h>
#include <fstream>
#include "ListTool2B.hpp"
#include "Const.hpp"
#include "Kunde.hpp"
#include "Funksjoner.hpp"
using namespace std;



class Kunder {
private:
	List* kundeListe;				//kundeliste
	int sisteKundeNr;				//int for å holde styr på antall kunder

public:
	Kunder();						//lager sorted Liste
	void handling();				//funksjon som lar brukeren velge handligsfunksjoner for kunde
	void kundeDisplay();			//kunde display funksjon
	void lesFraFil(ifstream & inn);	//kunde Lesfrafil funksjon
	void nyKunde();					//nykunde funksjon
	void slettKunde();				//slett kunde funksjon
    void endreKunde();				//endre kunde funksjon
	void skrivTilFil();				//skrivtilfil funksjon
    void skrivKundeTilFil(ofstream & utfil);
	bool kundeFins(int);			//sjekke at det er en kunde
    int kundeNrTilArrang();			//sende Kunde Nr
};

#endif /* Kunder_hpp */
