//  GRUPPE 38
//
//  Arrangementer.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Arrangementer.hpp"
#include "Arrangement.hpp"
#include "Funksjoner.hpp"
#include <iostream>
#include "Funksjoner.hpp"
#include "ListTool2B.hpp"
#include "Steder.hpp"

extern Steder stedbase;
extern Kunder kundebase;

Arrangementer::Arrangementer(){			//arrangment konstruktor
	sisteArr = 0;						//setter sisteArr til 0
    arrangementList=  new List(Sorted);	//lager ny liste for arrangementer
}


void Arrangementer::arrNytt() {		    //arrangeent ny funksjon
	
    char anvn[STRLEN], snvn[STRLEN];	//arragement navn og stedsnavn
    int  nr, ant;						//nr for opsettNr og  ant for sjekking av antall opsett
    Arrangement* arrangement;			//peker til arrangement
    List* liste;						//peker til liste

    les("Sted", snvn, STRLEN);
    ant = stedbase.antOppsett(snvn);	//henter antall opsett på valgt sted

    if (ant == -1)						//om returnerer -1 fant den ingen steder
        cout << "\n\n\tIngen arrangementsted med dette navnet!\n\n";
    else if (ant == 0)					//returnere 0 fant ingen opsett men fant sted
        cout << "\n\n\tIngen (stol)oppsett registrert på dette stedet!\n\n";
    else {								//ellers finner den ett eller fler opsett
        les("\n\tNytt arrangements navn", anvn, STRLEN);		//leser inn arrangement navn
        nr = les("Oppsettnummer", 1, ant);						//leser inn ønsket opsett Nr
        arrangement = new Arrangement(anvn, snvn, ++sisteArr);	//lager nytt arrangement og sender med navn og sted
        
        liste = stedbase.kopiOpps(snvn, nr);					//fyller en liste med et kopi av ønsket opsett
        arrangement->lesArr(snvn,liste);						//kopierer opsett
        arrangementList->add(arrangement);						//legger arrangement i lista

            //skrivArrTilFil();										//skriver arrangementet til fil
           // lesArrFraFil(anvn);									//får en error vi ikke greier å løse i skriv og les
	}

}
// Leser alle arrangementer fra fil
void Arrangementer::lesFraFil(ifstream & inn) {
    while(!inn.eof()) {
        char temp[NAVNLEN];
        int nr;
        
        inn >> nr; inn.ignore();
        inn.getline(temp, NAVNLEN);
        Arrangement* arrangement = new Arrangement(nr, temp, inn);
        cout << endl;
        arrangementList->add(arrangement);
    }
}


void Arrangementer::arrDisplay() { //display funksjon med 7 forskejllige alternativer
	
	cout << "\t(1) Display alle Arrangement\n";
	cout << "\t(2) Display alle som inneholder en Tekst\n";
	cout << "\t(3) Display alle som er på et bestemt sted\n";
	cout << "\t(4) Display alle som er på en bestemt dato\n";
	cout << "\t(5) Display alle av en bestemt type\n";
	cout << "\t(6) Display alle med en bestemt artist\n";
	cout << "\t(7) Display alle data om et spesefik arrangement\n";
	int valg = les("\nSkriv Tall: ", 1, 7);

	if (valg == 1){ //skriver ut alle arrangement
		Arrangement* hp;//hjelpepeker
		
		for (int i = 1; i <= sisteArr; i++){ //får igjenom alle arangementer
			hp = (Arrangement*)arrangementList->removeNo(i); //henter ut arrangement fra lista
			
			if (hp){
				hp->display();								//dispayer arrangementet
				arrangementList->add(hp);					//legger tilbake i lista
			}
		}
	}

	if (valg == 2){			//skriver ut arrangementer som inneholder søketekst
		char buffer[STRLEN]; //hjelpepeker og buffer for innlesning
		Arrangement* hp;
		cout << "\tOnsket soke tekst: ";
		cin.getline(buffer, STRLEN);		//leser in søketekst
		int finn = 0;						//int for å sjekke at minst 1 match
		for (int i = 1; i <= sisteArr; i++)
		{
			hp = (Arrangement*)arrangementList->removeNo(i); //henter ut arrangementer
			if (hp) {
				if (hp -> sjekkTekst(buffer)) { //sjekker at tekst matcher
					hp-> display(); //displayer arrangmenetet
					++finn; //øker int får å ungå feilmeldning
				}
			}
			arrangementList->add(hp); //leger tilbake i lista
			
		}
		if (finn == 0) //hvis ingen matcher finnes skrives det ut
			cout << "\tIngen navn som matcher\n";
	}
	if (valg == 3){ //skriver ut alle arrangmeenter på et bestemt sted
		char buffer[STRLEN]; Arrangement* hp;	//hjelpepeker og buffer for innlesning
		cout << "\tØnsket Sted: ";				//skriver inn ønsket sted
		cin.getline(buffer, STRLEN); 
		int ant = arrangementList->noOfElements(); //henter ut antall arrangementer
		for (int i = 1; i <= ant; i++){
			hp = (Arrangement*)arrangementList->removeNo(i);//henter ut arrangement
			if (hp->likSted(buffer)) {						//hvis sted matcher så displayer arrangementet
				hp->display();
			}
			arrangementList->add(hp);	//leger tilbake i lista			
		}
	}
	if (valg == 4){					//displayer alle arrangementer som er på samme Dato
		Arrangement* hp; //hjelpepeker
		int dato = les("\nDato: ", MINDATO, MAXDATO);				//leser inn dato
		int ant = arrangementList->noOfElements();
			for (int i = 1; i <= ant; i++){
				hp = (Arrangement*)arrangementList->removeNo(i);	//henter ut arrangementer
				if (hp->likDato(dato)){								//displayer om datoen er lik
					hp->display();
				}
				arrangementList->add(hp);							//legger tilbake arrangementer
			}
		//if (ingen arrangementer) cout << "ingen arrangementer her";
	}
	if (valg == 5){					//dispalyer alle arrangementer av samme type
		Arrangement* hp;
		cout << "Velg Arrangement type\n"
			<< "\t1 = Musikk\n"
			<< "\t2 = Theater\n"
			<< "\t3 = show\n"
			<< "\t4 = kino\n"
			<< "\t5 = sport\n"
			<< "\t6 = familie\n"
			<< "\t7 = festival\n";
		int valg = les("Valg: ", 1, 7); //bruker leser inn ønsket type
		int nr = arrangementList->noOfElements(); //finner antall arrangementer
		for (int i = 1; i <= nr; i++) {
			hp = (Arrangement*)arrangementList->removeNo(i); //henter ut arrangementer
			if (hp->enumType(valg)) {						//displayer arrangmeenter om enum er lik	
				hp->display();
				arrangementList->add(hp);
			}
			
		}

	}
	if (valg == 6){						//displayer alle arragnementer med lik artist
		Arrangement* hp; char buffer[STRLEN];	//hjelpepeker og buffer for innlesning
		cout << "\nArtistNavn: ";	
		cin.getline(buffer, STRLEN);				//leser artistnavn
		int ant = arrangementList->noOfElements();	//finner antall
		for (int i = 1; i <= ant; i++){
			hp = (Arrangement*)arrangementList->removeNo(i);	//henter ut arrangmeneter
			if (hp->likArtist(buffer)){							//sjekker om artist er lik og displayer
				hp->display();	
				
			}
			arrangementList->add(hp);							//legger tilbake artist
		}
	}
	if (valg == 7){				 //skriver ut all informasjon om spesefikt arrangement
        								//hjelpepeker og buffer for innlesning
        Arrangement* arrangement;
		
        int nr = les("Hvilket arrangement vil du se? (Nr)", 1 , MAXAAR); //skriver inn arrangemnet nummer
		
        arrangement = (Arrangement*) arrangementList->removeNo(nr); //henter valgt arrangement
        if (arrangement) {				//hvis finner fram displayes informasjonen inkludert opsett	
            arrangement->display();
            arrangement->info();
            arrangementList->add(arrangement);						  //legger tilabke arrangemnetet
        }
        else
            cout << "Fant ikke arrangementet\n";
    }
	else
		cout << "\nSkriv ny kommando: ";

}
void Arrangementer::kjopArrangement() {			//funksjon for å kjøpe bilett(er) til arrangemnet
    
    Arrangement* arrangement;		//hjelpepeker og buffer for innlesning
	char buffer[STRLEN];
    int temp = 0, antArr, kundeNr;	//int for antall arrangement og kundeNr
    
    kundeNr = kundebase.kundeNrTilArrang();     // Leser kundeNr  
    les("Arrangement navn: ", buffer, STRLEN);  // Leser arr navn
    antArr = arrangementList->noOfElements();   // antall eksisterende arr
    
    for (int i = 1; i <= antArr; i++) {         // Looper gjennom alle arr
        arrangement = (Arrangement*) arrangementList->removeNo(i);  // Henter arr nr i
        arrangementList->add(arrangement);      // adder arr tilbake i listen
        
        if(arrangement->validName(buffer)) {    // Hvis navnematch, break loop
            temp = i;   break;                  // Sette temp til i for riktig index
        }
    }
    
    if (antArr == 1){                           // Hvis kun 1 arr
        arrangement = (Arrangement*)arrangementList->removeNo(antArr);//Hent eneste arr
        if (arrangement->likNr(temp))           // Sjekke om arrnr er rikti
            arrangement->seteHandling
			(kundeNr); // Bestlling (HUSK AT MAN SKAL KUNNE VELGE VRIMLESONE)
        else
            cout << "Arrangementet finnes ikke\n";
        
        arrangementList->add(arrangement);      // Adder arr tilbake i liste
        
    }
    
    else if(antArr > 1){                        // Om det finnes flere en 1 arr
        arrangement = (Arrangement*)arrangementList->removeNo(temp);    // Hent temp-arr
        if (arrangement->likNr(temp))           // Sjekke om nr eksisterer
            arrangement->seteHandling(kundeNr); // Bestlling
        else
            cout << "Arrangementet finnes ikke\n";
        
        arrangementList->add(arrangement);      // Adder arr tilbake i liste
            
    }
    else
        cout << "ingen arrangementer\n";
    
	

}

/*
void Arrangementer::kjopArr(char* arrnvn, int kundeNr) {
	char buffer[STRLEN];
	int antArr = arrangementList->noOfElements();
	
    Arrangement* arrangement = (Arrangement*)arrangementList->remove(arrnvn);
    les( "arrangementet holdes på flere steder, velg sted: ",buffer, STRLEN);
	for (int i = 1; i < antArr; i++)
	{

	}
 
	//hp->kjøpbilett(kundeNr);
	arrangementList->add(arrangement);
}
*/
void Arrangementer::handling(){
    char onske = les();
    switch(onske){
	case 'D': arrDisplay();      break; //kjører arrangmenet dispay funksjon
	case 'N': arrNytt();		 break;	//kjører arrangement ny funksjon
	case 'K': kjopArrangement(); break;	//kjører arrangement kjøp funksjon
	case 'S': arrSlett();		 break;	//kjører arrangement slett funksjon
    default:    cout<<"Error"; break;   //feilmeldning

    }

}

Arrangementer :: ~Arrangementer() {		//destructor
	delete arrangementList;
}


// Skriver alle arrangementer til fil
void Arrangementer::skrivTilFil() {
    Arrangement* arrangement;						//hjelpepeker
    int i, ant = arrangementList->noOfElements();	//henter antall arrangmeneter
    
    ofstream utfil3("ARRANGEMENT.DTA");
    for (i = 1; i <= ant; i++) {			//skriver alle arrangementene til fil
        arrangement = (Arrangement*)arrangementList->remove(i); //henter arr
        arrangement->skrivTilFil(utfil3);		//skriver arrangement til liste
        arrangementList->add(arrangement);		//setter tilbake arrangementet
    }
    
}

void Arrangementer::lesArrFraFil(char* arrNvn) {
    Arrangement* arrangement;
    char buffer[STRLEN/2] = "ARR_";
    char tall[1];
    int arrNr;
    
    if (sisteArr > 0 && arrangementList->inList(arrNvn)) {
        arrangement = (Arrangement*) arrangementList->remove(arrNvn);
        arrNr = arrangement->hentArrNr();
        sprintf(tall, "%d", arrNr);
        strcat(buffer, tall);
        strcat(buffer, ".DTA");
        
        ifstream innfil4(buffer);
        cout << "Leser fra fil " << buffer << "........\n";
        arrangement->lesArrNrFraFil(innfil4);
        arrangementList->add(arrangement);
        arrangementList->displayList();
    }
    else
        cout << "Filen " << buffer << " ble ikke lest\n";
}

void Arrangementer::skrivArrTilFil() {      // Skriver ARR_NR.DTA
    Arrangement* arrangement;
    char buffer[NAVNLEN/2] = "ARR_";
    char tall[1];
    int nr = arrangementList->noOfElements();   // Henter nr med elementer
    
    // Henter arrangement med NR index
    arrangement = (Arrangement*) arrangementList->removeNo(nr);
    arrangementList->add(arrangement);      // adder tilbake i liste
    if (arrangement) {

        sprintf(tall, "%d", nr);            // gjør om nr til char
        strcat(buffer, tall);               // adder nr til ARR_
        strcat(buffer, ".DTA");             // adder .DTA til fila
        ofstream utfil4(buffer);
        cout << "Skriver til filen ARR_" << nr << ".DTA.......\n";
        arrangement->skrivArrNrTilFil(utfil4);  // skriv til ARR_NR.DTA
    }
    else
        cout << "Kunne ikke skrive til ARR_" << nr << ".DTA\n";
    
    
}

void Arrangementer::arrSlett() {		//funksjon for sletting av arrangement

	int nr, antArr;
	char arrBuffer[NAVNLEN / 2] = "ARR_";   // Const for ARR
	char tall[1];
    Arrangement* arrangement = nullptr;
    
    antArr = arrangementList->noOfElements();   // Henter antall i listen
    cout << "Alle tilgjengelige arrangementer:" << endl;
    arrangementList->displayList();
    nr = les ("Arrangement NR du vil slette", 1, antArr);
    
	if (!arrangementList->isEmpty() && arrangementList->noOfElements() < 100) {
        
        // Henter riktig arr NR
        arrangement = (Arrangement*) arrangementList->removeNo(nr);
        
        // Hvis arr eksisterer
        if (arrangement) {
            sprintf(tall, "%d", nr);    // gjøre tall om til char
            strcat(arrBuffer, tall);    // adde tall på buffer
            strcat(arrBuffer, ".DTA");  // adde DTA på buffer
            
            if( remove( arrBuffer ) != 0 )  // Fjerne filnavn
                perror( "Error deleting file" );
            else {
                puts( "File successfully deleted" );
                sisteArr--;
            }
        }

		//arrangementList->destroy(nr);
	}
	else
		cout << "Elementet fantes ikke i listen" << endl;
}
