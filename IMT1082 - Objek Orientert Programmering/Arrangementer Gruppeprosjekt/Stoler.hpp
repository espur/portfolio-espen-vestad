//  Gruppe 38
//
//  Stoler.hpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifndef Stoler_hpp
#define Stoler_hpp

#include "Sone.hpp"
#include <stdio.h>
#include <fstream>
#include "Enum.h"
using namespace std;

class Stoler : public Sone {
private:
	int rader;								//antall rader i sonen
	int seter;								//antall seter på hver rad
    int stolOppsett[MAXRADER][MAXSETER];	//stolopsett
    int** billett;							//dobbelpeker til billett
public:
	
    Stoler(char* nvn, soneType type);		//kosntruktor til stoler
    Stoler(char* nvn, ifstream & inn);		
    void display();							//display funksjon for stoler							
	void kjopPlass(int, int);				//funksjon for å kjøpe plass
    void skrivTilFil(ofstream & ut);		//skrivtilfil
    Stoler(Stoler & s);						//kopierings konstruktor
    void soneMatrise();						//skrive sonematrise
    int returnereRad();						//funksjon returnerer Rad
    int returnereSete();					//funksjon returnerer sete
    void lesStolersData(ifstream & inn);    // For å lese ARR_NR data
};


#endif /* Stoler_hpp */
