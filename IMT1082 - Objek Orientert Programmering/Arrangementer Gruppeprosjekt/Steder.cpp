//  Gruppe 38
//
//  Steder.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Steder.hpp"
#include "Sted.hpp"
#include "Vrimle.hpp"
#include "Stoler.hpp"
#include "Const.hpp"

using namespace std;



extern Sted sted;


Steder::Steder() {
	stedListe = new List(Sorted);

}
Steder::~Steder() {
	delete stedListe;

}

void Steder::stedDisplay() {
	if (!(stedListe->isEmpty()))
		stedListe->displayList();       // Displayer alle steder
}

void Steder::lesFraFil(ifstream & inn) {
        char stedNavn[STRLEN];
        int antallSteder = 0;
        inn >> antallSteder; inn.ignore();
    
        for (int i = 1; i <= antallSteder; i++) {
            inn.getline(stedNavn, STRLEN);
            Sted* sted = new Sted(stedNavn, inn);
            stedListe->add(sted);
        
    }
    
}

void Steder::skrivTilFil() {
    /*
     void Steder :: skrivStederTilFil(){
     ofstream utfil("STEDER.dta");
     
     if(utfil.is_open()){
     utfil << antSteder << endl;			// skriv antall steder
     for(int i = 1; i <= antSteder; i++){
     sted = (Sted*) steder -> removeNo(i);
     sted -> skrivTilFil(utfil);
     steder -> add(sted);
     }
     }
     }
     */
	int i, ant = stedListe->noOfElements();

	Sted* sted;
	ofstream utfil2("STEDER.DTA");

	for (i = 1; i <= ant; i++) {
		sted = (Sted*)stedListe->removeNo(i);
        stedListe->add(sted);
		sted->skrivTilFil(utfil2);
		
	}
}


// Kjører funksjon ut i fra bokstav-valg nr to
void Steder::stedsHandling(char v) {
    char onske = les();
    if (v == 'S') {
        switch (onske) {
            case 'D':   stedDisplay();              break;
            case 'N':   stedNytt();                 break;
            case 'S':   stedFjern();                break;
            default:    cout << "ugyldig kommando\n"; break;
        }
    }
    else {
        switch (onske) {
            default:    cout << "ugyldig kommando\n"; break;
        }
    }
}

// Kjører oppsett handlinger
void Steder::oppsettHandling(char v) {
    char onske = les();
    if (v == 'O') {
        
        switch (onske) {
            case 'D':   oppsettDisplay();
                break;
                
            case 'N':   oppsettNytt();
                break;
                
            case 'E':   oppsettEndre();
                break;
                
            default:    cout << "ugyldig kommando\n"; break;
                
        };
    }
}

void Steder::stedNytt() {
	Sted* tempSted;
	char temp[STRLEN];
	cout << "\nStedets Navn: ";
	cin.getline(temp, STRLEN);
	tempSted = new Sted(temp);      // adder nytt sted
	stedListe->add(tempSted);

}

void Steder::stedFjern() {
	char stedNavn[STRLEN];
	les("Stedsnavn som skal fjernes: ", stedNavn, STRLEN);

	if (stedListe->destroy(stedNavn))   // Sletter fra liste og memory
		cout << "Sted  " << stedNavn << "er fjernet" << endl;
    else
        cout << "Sted navn eksisterer ikke";

}



bool Steder::erIListe(char sted[]) {
    return stedListe->inList(sted);
}

bool Steder::displayElement(char sted[]) {
    return stedListe->displayElement(sted);
}

void Steder::oppsettDisplay() {
    char buffer[STRLEN];
    les("stedsnavn: ", buffer, STRLEN);
    
    if (stedListe->inList(buffer)) { // finner elemente i liste
        Sted* sted = (Sted*) stedListe->remove(buffer);
        sted->oppsettDisplay();
        stedListe->add(sted);
    } else {
        cout << "stedsnavn du har gitt eksistere ikke!";
    }
}

void Steder::oppsettNytt(){         // Legge til et nytt oppsett i et gitt sted
    char buffer[STRLEN];
    les("stedsnavn: ", buffer, STRLEN);
    
    if(stedListe->inList(buffer)){
        Sted* sted = (Sted*) stedListe->remove(buffer);
        sted->display();
        
        sted->nyttOppsett();
        
        stedListe->add(sted);
    }
    else{
        cout << "steds finns ikke! \n";
    }

}

void Steder::oppsettEndre() {       // Endre et oppsted i et gitt sted
    char buffer[STRLEN];
    les("Stedsnavn", buffer, NAVNLEN);
    
    if(stedListe->inList(buffer)) {
        Sted* tempSted = (Sted*) stedListe->remove(buffer);
        stedListe->add(tempSted);
        tempSted->endreOppsett();
    }
    else {
        cout << "stedet finnes ikke! \n";
    }
}



List* Steder::kopiOpps(char* opps, int ant) {   // Kopiere et oppsett på ett sted
	List* liste = nullptr;
	Sted* sted;
	if ((sted = (Sted*)stedListe->remove(opps))) {
		liste = sted->copycat(ant);
		stedListe->add(sted);
	}
	return liste;       // Returnerer listen med soner i gitt oppsett
}

int Steder::antOppsett(char* sted) {    // Henter ant oppsett på sted
	
	int ant;
	if (stedListe->inList(sted)) {
		Sted* hp;
		hp = (Sted*)stedListe->remove(sted);
		ant = hp->antOpps();
        stedListe->add(hp);
		if (ant > 0)
			return ant;
		else
			return 0;
	}
	else
		return -1;
}

void Steder::hentOppsett(char* stednvn) {   // henter ant oppsett på sted

    if (stedListe->inList(stednvn)) {
        Sted* sted = (Sted*) stedListe->remove(stednvn);
        
        if (sted) {
            sted->display();
            cout << "I hvilket oppsett vil du se billettsalg?\n";
            sted->oppsettDisplay();
        }
        else
            cout << "Fant ikke registrert sted for dette arrangementet\n";
        stedListe->add(sted);
    }
    else {
        cout << "Stedet finnes ikke\n";
    }
}

int Steder::returnOppsett(char* stednvn) {
    int antOpps = 0;
    if (stedListe->inList(stednvn)) {
        Sted* sted = (Sted*) stedListe->remove(stednvn);
        stedListe->add(sted);
        antOpps = sted->antOpps();
        
    }
    else
        cout << "Stedet finnes ikke\n";
    
    return antOpps;
}

// Henter en soneliste i ett oppsett og skriver innholdet til fil
void Steder::hentOppsettListe(char* stednvn, int index, ofstream & ut) {
    List* liste = nullptr;
    int sjekk;
    if (stedListe->inList(stednvn)) {               // hvis sted er i listen
        Sted* sted = (Sted*) stedListe->remove(stednvn);    // hent sted
        liste = sted->returnListe(stednvn, index);  // retur liste med soner
        stedListe->add(sted);
        
        // loope gjennom alle soner og skriver dets data til fil
        int antSoner = liste->noOfElements();
        for (int i = 1; i <= antSoner; i++) {
            Sone* sone = (Sone*) liste->removeNo(i);
            liste->add(sone);
            sjekk = sone->hentSoneType();
            
            // Hvis "stoler"
            if (sjekk) {
                Stoler* stoler = (Stoler*) liste->removeNo(i);
                stoler->skrivTilFil(ut);    // skrivStolerTilFil
                liste->add(stoler);
            }
            else {
                Vrimle* vrimle = (Vrimle*) liste->removeNo(i);
                vrimle->skrivTilFil(ut);    // Skriv vrimleTilFil
                liste->add(vrimle);
            }
        }
    }
}

// Henter en liste for et oppsett for å loope gjennom alle sonene
void Steder::hentOppsettListeForSone(char* stednvn, int index) {
    List* liste = nullptr;
    
    if (stedListe->inList(stednvn)) {
        Sted* sted = (Sted*) stedListe->remove(stednvn);
        liste = sted->returnListe(stednvn, index);
        stedListe->add(sted);
        
        int antSoner = liste->noOfElements();
        for (int i = 1; i <= antSoner; i++) {
            Sone* sone = (Sone*) liste->removeNo(i);
            liste->add(sone);
            }
    }
}
