//  GRUPPE 38
//
//  Kunder.cpp
//  main_project_c++
//
//  Created by xxxhamcho on 3/11/19.
//  Copyright © 2019 xxxhamcho. All rights reserved.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "Kunder.hpp"
#include "Kunde.hpp"
#include "Funksjoner.hpp"
#include "ListTool2B.hpp"
#include <cstring>
using namespace std;

Kunder::Kunder() {
	kundeListe = new List(Sorted);
	sisteKundeNr = 0;
}

void Kunder::kundeDisplay() {
	cout << "Se alle kunder, alle kunder med gitt navn, eller gitt kundenr? (A, B, C)";
	char onske = les();

	if (!kundeListe->isEmpty()) {         // Så lenge ikke-tom liste

		if (onske == 'A') {                 // Displayer alle kunder
			kundeListe->displayList();
			cout << "\n\n";
		}

		else if (onske == 'B') {                                        // Displayer kun kunder med 'navn'
			char temp[NAVNLEN];
			les("Angi navn", temp, NAVNLEN);                            // Leser inn navn

			for (int i = 1; i <= sisteKundeNr; i++) {                   // Går gjennom hver kunde
				Kunde* tempKunde = (Kunde*)kundeListe->removeNo(i);     // Henter hver kundeindeks

				if (!tempKunde->sjekkNavn(temp))                        // Hvis kundenavn matcher, printes de
					tempKunde->display();

				cout << endl;
				kundeListe->add(tempKunde);                             // Pga 'removeNo'
			}
		}

		else if (onske == 'C') {               // Displayer kunder med et gitt nr
			int kundeNr = les("Velg kundenummer", 1, sisteKundeNr);
			kundeListe->displayElement(kundeNr);
		}

		else                                   // Om ikke A, B eller C
			cout << "Ugyldig kommando\n\n";
	}

	else                                       // Dersom listen er tom
		cout << "Kundelisten er tom\n\n";
}

void Kunder::lesFraFil(ifstream & inn) {		//lesfrafil funksjon

    while(!inn.eof()) {				//om filen finnes
        int nr;
        inn >> nr; inn.ignore();
        sisteKundeNr = nr;
        Kunde* kunde = new Kunde(nr, inn);
        //kunde->display();
        cout << endl;
        kundeListe->add(kunde);		//leg til kunde i liste
    }

}

void Kunder::handling() {		//ønsket handling med Kunde Funksjoner

	char onske = les();
	switch (onske) {
	case 'D': kundeDisplay();               break;
	case 'N': nyKunde();                    break;
    case 'E': endreKunde();                 break;
	case 'S': slettKunde();                 break;
	default:  cout << "ugyldig kommando\n"; break;
	}
}


void Kunder::skrivTilFil() {		//skrivtilfil funksjon
	ofstream utfil1("KUNDER.DTA");	
    
    if (!kundeListe->isEmpty()) {	//skriver til fil om det finnes kunder
        for (int i = 1; i <= kundeListe->noOfElements(); i++) { //går igjenom alle kunder
            Kunde* kunde = (Kunde*)kundeListe->removeNo(i);
            kundeListe->add(kunde);
            kunde->skrivTilFil(utfil1);
            
        }
	}
}

void Kunder::nyKunde() {		//ny kunde funksjon
	Kunde* tempKunde = new Kunde(++sisteKundeNr); //lager hjelpepeker til Ny kunde
	kundeListe->add(tempKunde);					  //legger til kunden i kundeliste

}
void Kunder::slettKunde()		//slettkunde funksjon
{
	int kundeNr;				//bruker kundenr og hjelpepeker til kunde
	Kunde* tempKunde;
    kundeNr = les("Velg kundeNr du vil fjerne", 1, sisteKundeNr);

	if (!kundeListe->isEmpty())   // er liste ikke tom?
	{
		tempKunde = (Kunde*)kundeListe->removeNo(kundeNr); // hent

		kundeListe->destroy(kundeNr);      //fjern
		cout << "Kunde nummer:  " << kundeNr << " er fjernet \n";

	}
	else        // melding
		cout << "Kundelisten er tom" << endl;
}

void Kunder::endreKunde() {	//endre kunde funksjon
    int kundeNr;		//bruker kundeNr og hjelpepeker til kunde
    Kunde* tempKunde;
    
    if (!kundeListe->isEmpty()) {  //list er ikke tomt
        kundeNr = les("Skriv kundenummer du vil fjerne", 1, sisteKundeNr);
        
        tempKunde = (Kunde*)kundeListe->removeNo(kundeNr);//hent
        kundeListe->destroy(kundeNr); //fjern
        
        tempKunde = new Kunde(kundeNr); //lager ny kunde og legger inn i liste
        kundeListe->add(tempKunde);
        cout << "Kunde nr " << kundeNr << " er endret." << endl;
    }
    else
        cout << "Kundelisten er tom" << endl; //feilmeldning
}

bool Kunder::kundeFins(int kNr) { //bool funksjon sjekker om kunden finnes i liste
	if (kundeListe->inList(kNr))
		return true;
	else
		return false;
}

int Kunder::kundeNrTilArrang(){  //returnere kunde Nr som skal brukes i arrangement kjop
 

    int kundeNrTA = les("Kunde Nummer: ", 0 , 100); //skriver inn kundeNR
    
    if(kundeListe->inList(kundeNrTA)){ //finenr kunden i lista
    Kunde* kunde = (Kunde*)kundeListe->remove(kundeNrTA); //henter ut kundens informasjon
    kundeListe->add(kunde);								// og legger tilbake i lista
    return kundeNrTA;									//returnerer kundeNr
    }
    
    else{//feilemldninger
        cout << "kunde eksisterer ikke! ((P)rov pa nytt/ (Q)uit): \n";
        char valg = les();

        switch (valg) {
            case 'P': kundeNrTilArrang();
                break;
                
            case 'Q': break;
            default:
                cout << "kunde eksisterer ikke! ((P)rov pa nytt/ (Q)uit): \n";

                break;
        }
    }
    return 0;
}
