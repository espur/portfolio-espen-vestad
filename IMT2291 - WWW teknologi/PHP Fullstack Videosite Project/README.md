# Prosjektdeltakere #
- Espen Taftø Vestad
- Bjørnar Larsen
- Simen Bai

# Oppgaveteksten # 
Oppgaveteksten ligger i [Wikien til fjorårets repository](https://bitbucket.org/okolloen/imt2291-prosjekt1-2019/wiki/Home).

# Rapporten #
Rapporten ligger på prosjektets wiki: https://bitbucket.org/espur/imt2291-prosjekt1-2020/wiki/Home