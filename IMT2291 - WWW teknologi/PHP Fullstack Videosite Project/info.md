# Prosjekt 1 www
## Brukere
- Studenter
- lærere/forelesere
- administratorer

## Funksjonalitet:
- Opplastning av videoer (tittel, beskrivelse, thumbnail?, emne, tema, foreleser)
- Søking etter videoer (basert på emne, tema/tittel, foreleser)
- Opprettelse av spillelister (tittel, beskrivelse, foreleser, antVideoer, spilleliste-thumbnail)
- Søking ettet spillelister (foreleser, emne, tema)
- Kommentarer på videoer
- Rangering av videoer for studenter (0 - 5 stjerner)
- Display av videoer innenfor studentens abonnement (forside etter innlogging)
- Oversikt over alle videoer og spillelister en lærer har opprettet (læreroversikt)
- Organisering av spillelister for lærer (legge til, endre, slette videoer fra liste, endre rekkefølge
- Hvem som helst skal ha mulighet til å registrere seg som student
- Registrerings form skal ha checkbox «jeg er lærer»
- Admin side som godkjenner/avviser brukere som prøver å registrere seg som lærere


## Hvem kan gjøre hva
### Studenter:
- abonnere på spillelister
- Kommentere videoer
- Rangere videoer

### Lærere:
- Opprette spillelister
- Organisere innhold i spillelister
- Slette/endre spillelister
- Laste opp videoer
- Endre/slette videoer
- Kommentere videoer

### Admin
- Egen side for å godkjenne/avvise lærer-registreringer.
- Egen siden for å kunne søke opp andre aminer i brukerdatabasen og gi/fjerne admin tilgang på brukere


## Klasser:
- Brukere
- Videoer
- Spillelister
- Administrator

# NOTES
- ALL HTML MED TWIG
- UNIT TEST FOR KLASSER OG METODER
    - opprettelse av spillelister med innlegging av 3 videoer
    - endring av rekkefølge 
    - Kort rapport i readme.md