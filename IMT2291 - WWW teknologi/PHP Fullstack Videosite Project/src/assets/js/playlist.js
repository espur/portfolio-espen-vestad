Sortable.create(playlist, {
    group: 'shared',
    multiDrag: true,
    selectedClass: "selected",
    animation: 100,
    store: {
          get: function (sortable) {
              var order = sortable.toArray();
              document.getElementById("order").value = order;
          },
          set: function (sortable) {
              var order = sortable.toArray();
              document.getElementById("order").value = order;
          }
      }
  });
  
  Sortable.create(all, {
    group:'shared',
    multiDrag: true,
    selectedClass: "selected",
    animation: 100
  });