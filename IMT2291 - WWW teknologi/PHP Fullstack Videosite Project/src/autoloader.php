<?php



require_once '../../vendor/autoload.php';

// config
require_once 'classes/config.php';

// utils
require_once 'classes/Utils.php';

//database
require_once 'classes/DB.php';

//Users
require_once 'classes/User.php';

//Permissions
require_once 'classes/Permission.php';

// pages
require_once 'classes/loadPages.php';

// Videos
require_once 'classes/Video.php';

// Playlist
require_once 'classes/Playlist.php';

$user = new User();
$playlist = new Playlist();
$video = new Video();
