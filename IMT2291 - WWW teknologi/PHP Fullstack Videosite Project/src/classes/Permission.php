<?php

class Permission
{

    /**
     * @param $permission  Which permission to query
     * @param $userRole  Which userrole the user has
     * @return bool If it has permissions or not
     */
    public static function hasPermission($permission, $userRole)
    {
        $db = DB::getDBConnection();
        $sql = 'SELECT permissionUserTypeID 
                FROM permissionUserType 
                WHERE permissionID = (
                    SELECT permissionID 
                    FROM permission 
                    WHERE permissionName = ":permissionName") 
                AND userTypeID = ":userRole"';
        $sth = $db->prepare($sql);
        $sth->bindParam(':userRole', $userRole);
        $sth->bindParam(':permissionName', $permission);
        $sth->execute();

        if ($sth->rowCount() != 0) {
            return true;
        }
        return false;
    }
}