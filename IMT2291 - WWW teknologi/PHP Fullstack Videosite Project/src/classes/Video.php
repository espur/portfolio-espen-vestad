<?php


Class Video
{
        //Function to add new video, Returns videoID or 0
    public static function addVideo($results)
    {
        $db = DB::getDBConnection();
        $uid = User::getUserID();

        $sql = "INSERT INTO video 
        (title, description, thumbnailUrl, subject, theme, lecturer) 
        VALUES (:title, :description, :thumbnailUrl, :subject, :theme, :lecturer)";
                $sth = $db->prepare($sql);
                $sth->bindParam(':title', $results['title']);
                $sth->bindParam(':description', $results['description']);
                $sth->bindParam(':thumbnailUrl', $results['thumbnailUrl']);
                $sth->bindParam(':subject', $results['subject']);
                $sth->bindParam(':theme', $results['theme']);
                $sth->bindParam(':lecturer', $uid);
                $sth->execute();
        if ($sth->rowCount() == 1){
            $id = $db->lastInsertId();
            return ($id);
        }
        else return 0;
    }

        //Function to delete videos by id
    public static function deleteVideoByID($videoID)
    {
        $db = DB::getDBConnection();
        $sql = "DELETE FROM video WHERE videoID = :videoID";
        $sth = $db->prepare($sql);
        $sth->bindParam(':videoID', $videoID);
        $sth->execute();
    }

        //Function to update a video
    public static function updateVideo($updateResult)
    {
        $db = DB::getDBConnection();

        $sql = "UPDATE video SET title = :title,
        description = :description,
        thumbnailUrl = :thumbnailUrl,
        subject = :subject,
        theme = :theme
        WHERE videoID= :videoID";
                $sth = $db->prepare($sql);
                $sth->bindParam(':title', $updateResult['title']);
                $sth->bindParam(':description', $updateResult['description']);
                $sth->bindParam(':thumbnailUrl', $updateResult['thumbnailUrl']);
                $sth->bindParam(':subject', $updateResult['subject']);
                $sth->bindParam(':theme', $updateResult['theme']);
                $sth->bindParam(':videoID', $updateResult['videoID']);
                $sth->execute();
    }

        //Function to return a users videos
    function getUserVideos()
    {
        $db = DB::getDBConnection();
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }
        $uid = User::getUserID();

        $sql = 'SELECT * FROM `video` WHERE lecturer= :lecturer';
        $sth = $db->prepare($sql);
        $sth->bindParam(':lecturer', $uid);
        $sth->execute();

        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

        //Function to return video by id
    public static function getVideoByID($videoID)
    {
        $db = DB::getDBConnection();
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        $sql = "SELECT * FROM video WHERE videoID = :videoID";
        $sth = $db->prepare($sql);
        $sth->bindParam(':videoID', $videoID);
        $sth->execute();

        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public static function getVideoIDBySubjectOrTheme($subject, $theme) {
        $db = DB::getDBConnection();
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        $sql = 'SELECT "subject", "theme" FROM video WHERE ';
    }

        //Function for video search
    public static function searchForVideo($lecturer, $subject, $theme, $title)
    {
        $db = DB::getDBConnection();
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }
        
        $sql = "SELECT * FROM video WHERE";
        $extra = '';
        $current = 0;

        if(!empty($lecturer)) {
            $extra .= " lecturer = :lecturer";
            $current++;
        }
        if(!empty($subject)) {
            if($current > 0) {
                $extra .= " OR LOWER(subject) LIKE '%{$subject}%'";
            }
            else {
                $extra .= " LOWER(subject) LIKE '%{$subject}%'";
            }
            $current++;
        }
        if(!empty($theme)) {
            if($current > 0) {
                $extra .= " OR LOWER(theme) LIKE '%{$theme}%'";
            }
            else {
                $extra .= " LOWER(theme) LIKE '%{$theme}%'";
            }
            $current++;
        }
        if(!empty($title)) {
            if($current > 0) {
                $extra .= " OR LOWER(title) LIKE '%{$title}%'";
            }
            else {
                $extra .= " LOWER(title) LIKE '%{$title}%'";
            }
        }
        
        $sql .= $extra;
        $sth = $db->prepare($sql);
        $sth->bindParam(':lecturer', $lecturer);
        $sth->execute();

        $res = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $res;
    }

        //Function to get videos in a playlist
    public static function getVideosInPlaylist($playlistID)
    {
        $db = DB::getDBConnection();
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        $sql = "SELECT video.videoID, `title`, `description`, `thumbnailUrl`, `subject`, `theme`, `lecturer` 
        FROM video INNER JOIN playlistPosition ON video.videoID = playlistPosition.videoID 
        WHERE playlistPosition.playlistID = :playlistID";
            $sth = $db->prepare($sql);
            $sth->bindParam(':playlistID', $playlistID);
            $sth->execute();

        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

        //function to get videos not in a users playlist
    public static function getVideosNotInPlaylist($playlistID)
    {
        $db = DB::getDBConnection();
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }
        $uid = User::getUserID();

        $sql = "SELECT * FROM video WHERE lecturer = :lecturer AND video.videoID
        NOT IN (SELECT video.videoID FROM video INNER JOIN playlistPosition ON video.videoID = playlistPosition.videoID 
        WHERE playlistPosition.playlistID = :playlistID)";
            $sth = $db->prepare($sql);
            $sth->bindParam(':lecturer', $uid);
            $sth->bindParam(':playlistID', $playlistID);
            $sth->execute();

        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }


        //function for adding comment on a video
    public static function addComment($videoID, $comment){
        $db = DB::getDBConnection();
        $sql = "
            INSERT INTO videoComment 
                (userID, videoID, comment)
                VALUES (:userID, :videoID, :comment)";
        $sth = $db->prepare($sql);
        $sth->bindParam(":userID", User::getUserID());
        $sth->bindParam(":videoID", $videoID);
        $sth->bindParam(":comment", $comment);
        $sth->execute();
    }

        //function for adding rating on a video
    public static function addRating($videoID, $rating){
        $db = DB::getDBConnection();

        $sql = "INSERT INTO ratings (userID, videoID, score)
                VALUES (:userID, :videoID, :score)";
        $sth = $db->prepare($sql);
        $sth->bindParam(":userID", User::getUserID());
        $sth->bindParam(":videoID", $videoID);
        $sth->bindParam(":score", $rating);
        $sth->execute();
    }
}