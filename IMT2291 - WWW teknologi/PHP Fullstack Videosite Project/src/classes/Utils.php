<?php

class Utils
{
    public static function ERROR_MSG($error, $errorCode)
    {
        if (DEBUG) {
            Router::error($errorCode, 'something failed: ' . $error->getMessage());
        } else {
            //TODO: Maybe log the error?
            Router::error($errorCode, 'There was an error');
        }
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public static function inArray($fields, $haystack, $strict = false)
    {
        $matches = 0;

        foreach ($fields as $key => $value) {
            foreach ($haystack as $object) {
                $convertedObject = (array)$object;
                if (isset($convertedObject[$key]) && $convertedObject[$key] == $value) {
                    if (!$strict)
                        return true;
                    else
                        $matches++;
                }
            }
        }

        if (!$strict)
            return false;
        else
            return ($matches == count($fields));
    }

    public static function findFromArray($fields, $haystack)
    {
        foreach ($haystack as $object) {
            foreach ($fields as $key => $value) {
                $convertedObject = (array)$object;
                if (isset($convertedObject[$key]) && $convertedObject[$key] == $value) {
                    return $object;
                }
            }
        }

        return null;
    }
}