<?php

class User
{

    static $userID = -1;       // -1 = not signed in
    static $email = '';
    static $fullName = '';
    static $role = -1;          // 1 = student, 2 = lecturer, 3 = admin

    /*
    Init user obj.
    If a user is logged in, its relevant information will be selected from
    the database and assigned to relevant data members.
    */
    public function __construct()
    {
        $db = DB::getDBConnection();

        if(isset($_SESSION['userID'])) {
            self::$userID = $_SESSION['userID'];
            $sql = 'SELECT fullname, email FROM users WHERE userID = :userID';
            $sth = $db->prepare($sql);
            $sth->bindParam(':userID', self::$userID);
            $sth->execute();

            if($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                self::$email = $row['email'];
                self::$fullName = $row['fullName'];

                $sql = 'SELECT userTypeID FROM userType WHERE userID = :userID';
                $sth = $db->prepare($sql);
                $sth->bindParam(':userID', self::$userID);
                $sth->execute();

                if($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                    self::$role = $row['userTypeID'];
                }

            }
        }
    }

    /*
    The two following functions retrieves user fullName and userID based on
    a name in the search term
    */

    public static function getUsernameByName($name) {
        $db = DB::getDBConnection();

        $sql = 'SELECT fullName FROM users WHERE fullName = :fullName';
        $sth = $db->prepare($sql);
        $sth->bindParam(':fullName', $name);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public static function getUserIDByName($name) {
        $db = DB::getDBConnection();

        $sql = 'SELECT userID FROM users WHERE fullName = :fullName';
        $sth = $db->prepare($sql);
        $sth->bindParam(':fullName', $name);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    /*
    The following 4 functions returns relevant data about the user.
    */

    public static function getUserID()
    {
        return self::$userID;
    }


    public static function isAdmin()
    {
        return self::$role == 3;
    }

    public static function isLoggedIn()
    {
        return self::$userID > -1;
    }

    public static function whichRole()
    {
        return self::$role;
    }

    /*
    Lets an administrator change a specific user's role.
    */

    private static function changeRole($userRoleID, $userID)
    {
        $db = DB::getDBConnection();
        $sql = 'UPDATE userType
                SET userTypeId = :userRoleID
                WHERE userID = :userID';
        $sth = $db->prepare($sql);
        $sth->bindParam(':userRoleID', $userRoleID);
        $sth->bindParam(':userID', $userID);
        $sth->execute();

        if ($sth->rowCount() != 0) {
            return true;
        }
        return false;
    }

    /*
    The following functions are related to admin actions
    */

    public static function grantAdmin($userID)
    {
        return User::changeRole(3, $userID);
    }

    public static function revokeAdmin($userID)
    {
        return User::changeRole(1, $userID);
    }

    public static function grantTeacher($userID)
    {
        return User::changeRole(2, $userID);
    }

    public static function revokeTeacher($userID)
    {
        return User::changeRole(1, $userID);
    }
}