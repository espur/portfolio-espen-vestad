<?php

/**
 * Class Router
 *
 * Is a router class for routing the requests that comes into the application. This enables us to better control the
 * information leakage. It works by first adding all the pages into a register, and then checking if any of the paths matches the register.
 * Videos are the only thing that doesn't get routed. There is an exception for it in the .htaccess file
 */
class Router
{
    /**
     * @var array The paths that should be available
     */
    private static $paths = [];
    /**
     * @var array The assets that should be available. These are handled a little differently because of the MIME type.
     */
    private static $asset = [];

    /**
     * @param $path Path on the webpage
     * @param $handler The page that should be rendered
     * @param bool $permission The permission needed to access the page
     */
    public static function addGetPath($path, $handler, $permission = false)
    {
        self::$paths[] = new RouterPage("GET", $path, $handler, $permission);
    }

    /**
     * @param $path Path on the webpage
     * @param $handler The page that should be rendered
     * @param bool $permission The permission needed to access the page
     */
    public static function addTwigPath($path, $view, $permission = false)
    {
        Router::addGetPath($path, new class($view) extends Page {
            private $view;

            public function __construct($view)
            {
                $this->view = $view;
            }

            public function handle($args)
            {
                RouterPage::renderTwig($this->view);
            }
        }, $permission);
    }

    /**
     * @param $path Path on the webpage
     * @param $handler The page that should be rendered
     * @param bool $permission The permission needed to access the page
     */
    public static function addPostPath($path, $handler, $permission = false)
    {
        self::$paths[] = new RouterPage("POST", $path, $handler, $permission);
    }

    /**
     * @param $path Path to get the asset on
     * @param $location Location on server for the asset
     * @param $header The header that should be used. For controlling MIME type
     */
    public static function addAsset($path, $location, $header)
    {
        self::$asset[] = [$path, $location, $header];
    }

    //Handle incoming requests

    /**
     * @param $path Path of the request from user
     * @param $method Method of the request gotten from user
     */
    public static function handle($path, $method)
    {
        //If the path was found
        $foundPath = false;

        //Check if path matches asset path
        foreach (self::$asset as $asset) {
            if ($asset[0] == $path) {
                header($asset[2]);
                echo file_get_contents("." . $asset[1]);
                exit();
            }
        }

        //Go over each page, and see if it matches
        foreach (self::$paths as $page) {
            //Wrong method
            if ($method != $page->method)
                continue; // skips to next element in loop

            if (!is_array($page->path)) {
                $page->path = [$page->path];
            }

            foreach ($page->path as $pagePath) {
                if ($path == $pagePath) {
                    $foundPath = true;

                    if ($page->permission) {
                        global $user;
                        if (Permission::hasPermission($page->permission, $user->whichRole()) || !PERMISSION_ON) {
                            $page->handle();
                        } else {
                            self::error(403, "Permission denied");
                        }
                    } else {
                        $page->handle();
                    }
                    break;
                }
            }

        }

        if (!$foundPath) {
            self::error(404, "Page Not Found");
        }

        exit;
    }

    /**
     * @param $code Error code that should be returned
     * @param $message The message that should be returned
     */
    public static function error($code, $message)
    {
        http_response_code($code);

        $errorPage = Utils::findFromArray(["path" => ["error"]], self::$paths);
        // could render error page
        if ($errorPage != null) {
            $errorPage->handle($code . " - " . $message);
        } else {
            echo $message;
        }

        exit;
    }
}

/**
 * Class RouterPage
 *
 *  The class for handeling the different pages
 *
 */
class RouterPage
{
    /**
     * @var Which method should be used
     */
    public $method;
    /**
     * @var What path/paths it has
     */
    public $path;
    /**
     * @var The function that should be ran.
     */
    private $handler;
    /**
     * @var The permission needed to access the page
     */
    public $permission;
    /**
     * @var Twig loader
     */
    private static $loader;
    /**
     * @var twig file
     */
    private static $twig;

    /**
     * RouterPage constructor.
     * @param $method
     * @param $path
     * @param $handler
     * @param $permission
     */
    public function __construct($method, $path, $handler, $permission)
    {
        $this->method = $method;
        $this->path = $path;
        $this->handler = $handler;
        $this->permission = $permission;
    }

    // args allows us to pass an array with key => value for /:id and other things like that in the future

    /**
     * @param array $args
     * @return mixed
     */
    public function handle($args = [])
    {
        return $this->handler->handle($args);
    }

    /**
     *  Renders the file with the given array
     */
    public static function renderTwig($view, $array = [])
    {
        if (is_null(RouterPage::$loader)) {
            // twig
            self::$loader = new \Twig\Loader\FilesystemLoader('twig');
        }
        if (is_null(RouterPage::$twig)) {
            self::$twig = new \Twig\Environment(self::$loader, [
                //'cache' => './compilation_cache', // Only enable cache when everything works correctly
            ]);
        }
        self::$twig->addGlobal('role', User::whichRole());

        echo self::$twig->render($view, $array);
    }
}

/**
 * Class Page
 */
abstract class Page
{
    /**
     * @param $arguments
     * @return mixed
     */
    abstract public function handle($arguments);
}