<?php


Class Playlist
{
    public function addPlaylist($result)
    {
        $db = DB::getDBConnection();

        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        $uid = User::getUserID();

        $sql = "INSERT INTO playlist 
    (title, description, thumbnailUrl, lecturer) 
    VALUES (:title, :description, :thumbnailUrl, :lecturer)";
            $sth = $db->prepare($sql);
            $sth->bindParam(':title', $result['title']);
            $sth->bindParam(':description', $result['description']);
            $sth->bindParam(':thumbnailUrl', $result['thumbnailUrl']);
            $sth->bindParam(':lecturer', $uid);
            $sth->execute();

            if ($sth->rowCount() == 1) {

                //get playlist ID
            $id = $db->lastInsertId();

                //convert from string to array
            $order = explode(",", $result['order']);

                //variable for keeping track of index
            $index = 0;
            foreach ($order as $video) {
                $sql = "INSERT INTO playlistPosition 
                (videoID, playlistID, position) 
                VALUES (:videoID, :playlistID, :position)";
                        $sth = $db->prepare($sql);
                        $sth->bindParam(':videoID', $video);
                        $sth->bindParam(':playlistID', $id);
                        $sth->bindParam(':position', $index);
                        $sth->execute();
                if ($sth->rowCount() == 1){
                    $index++;
                }
            }
        }
    }

    public static function updatePlaylist($updateResult)
    {
        $db = DB::getDBConnection();

        $sql = "UPDATE playlist SET title = :title,
        description = :description, 
        thumbnailUrl = :thumbnailUrl
        WHERE playlistID = :playlistID";
            $sth = $db->prepare($sql);
            $sth->bindParam(':title', $updateResult['title']);
            $sth->bindParam(':description', $updateResult['description']);
            $sth->bindParam(':thumbnailUrl', $updateResult['thumbnailUrl']);
            $sth->bindParam(':playlistID', $updateResult['playlistID']);
            $sth->execute();


        $sql = "DELETE FROM playlistPosition WHERE playlistID = :playlistID";
            $sth = $db->prepare($sql);
            $sth->bindParam(':playlistID', $updateResult['playlistID']);
            $sth->execute();

            $order = explode(",", $updateResult['order']);

                //variable for keeping track of index
            $index = 0;
            foreach ($order as $video) {
                $sql = "INSERT INTO playlistPosition 
                (videoID, playlistID, position) 
                VALUES (:videoID, :playlistID, :position)";
                        $sth = $db->prepare($sql);
                        $sth->bindParam(':videoID', $video);
                        $sth->bindParam(':playlistID', $updateResult['playlistID']);
                        $sth->bindParam(':position', $index);
                        $sth->execute();
                $index++;
            }
    }

    public static function deletePlaylistByID($playlistID)
    {
        $db = DB::getDBConnection();

        $sql = "DELETE FROM playlist WHERE playlistID = :playlistID";
                $sth = $db->prepare($sql);
                $sth->bindParam(':playlistID', $playlistID);
                $sth->execute();
    }

    public function getUserPlaylist()
    {
        $db = DB::getDBConnection();
        $uid = User::getUserID();

        $sql = "SELECT * FROM playlist WHERE lecturer = :lecturer";
        $sth = $db->prepare($sql);
        $sth->bindParam(':lecturer', $uid);
        $sth->execute();

        $results = $sth->fetchAll(PDO::FETCH_ASSOC);




        return $results;
    }

    public static function getPlaylistByID($playlistID)
    {
        $db = DB::getDBConnection();

        $sql = "SELECT * FROM playlist WHERE playlistID = :playlistID";
        $sth = $db->prepare($sql);
        $sth->bindParam(':playlistID', $playlistID);
        $sth->execute();

        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public static function searchForPlaylist($lecturer, $title)
    {
        $db = DB::getDBConnection();

        $sql = "SELECT * FROM playlist WHERE";
        $extra = '';
        $current = 0;
        
        if(!empty($lecturer)) {
            $extra .= " lecturer = {$lecturer}";
            $current++;
        }
        
        if(!empty($title)) {
            if($current > 0) {
                $extra .= " OR LOWER(title) LIKE '%{$title}%'";
            }
            else {
                $extra .= " LOWER(title) LIKE '%{$title}%'";
            }
        }
        
        $sql .= $extra;
        $sth = $db->prepare($sql);
        $sth->execute();
        
        $res = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

    public static function subscribe($playlistID){
        $db = DB::getDBConnection();

        $sql = "
            INSERT INTO subscription 
                (playlistID, userID)
                VALUES (:playlistID, :userID)";
        $sth = $db->prepare($sql);
        $sth->bindParam(":playlistID", $playlistID);
        $sth->bindParam(":userID", User::getUserID());
        $sth->execute();
    }

    public static function unsubscribe($subscribeID){
        $db = DB::getDBConnection();

        $sql = "
            DELETE FROM subscription
                WHERE subscription = :subscriptionID";
        $sth = $db->prepare($sql);
        $sth->bindParam(":userID", $subscribeID);
        $sth->execute();
    }

}