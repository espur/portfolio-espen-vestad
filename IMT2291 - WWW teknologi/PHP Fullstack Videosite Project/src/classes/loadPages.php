<?php
// router
require_once 'Router.php';


foreach (getFiles("/../views/", '/^.+\.php$/i') as $info){
    require_once $info[0];
}

foreach (getFiles("/../assets", "/^(.+\.css)$/is") as $info){
    $path = preg_replace('/^\/var\/www\/html\/src/','',realpath($info[0]));
    Router::addAsset($path, $path, "Content-type: text/css; charset: UTF-8");
}

foreach (getFiles("/../assets", "/^(.+\.js)$/is") as $info) {
    $path = preg_replace('/^\/var\/www\/html\/src/','',realpath($info[0]));
    Router::addAsset($path, $path, "Content-type: text/javascript; charset: UTF-8");
}

function getFiles($directory, $regex){
    $Directory = new RecursiveDirectoryIterator(__DIR__.$directory);
    $Iterator = new RecursiveIteratorIterator($Directory);
    return new RegexIterator($Iterator, $regex, RecursiveRegexIterator::GET_MATCH);
}