<?php

class DB
{
    private static $db = null;
    private $dbh = null;

    function __construct()
    {
        try {
            $this->dbh = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        } catch (PDOException $e) {
            Utils::ERROR_MSG($e, 500);
        }
    }

    public static function getDBConnection()
    {
        if (DB::$db == null) {
            DB::$db = new self();
        }
        return DB::$db->dbh;
    }

    function __deconstruct()
    {
    }
}