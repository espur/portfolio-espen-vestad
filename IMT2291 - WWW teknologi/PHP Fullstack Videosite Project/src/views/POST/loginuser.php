<?php

$loginUserPage = new LoginUser();

//Post path that sets a user session.
Router::addPostPath('/loginuser', $loginUserPage);

class LoginUser extends Page {
    
    public function handle($args) {
        
        // Create and confirm DB connection
        $db = DB::getDBConnection();
        
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        // Check if user is logged in and if 'login' button is pressed.
        if(!User::isLoggedIn()) {

            if(isset($_POST['login'])) {
                
                $sql = 'SELECT userID, email, passwordHash
                FROM users WHERE email = :email';
                $sth = $db->prepare($sql);
                $sth->bindParam(':email', $_POST['email']);
                $sth->execute();

                if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {

                    // Check if password matches with hash and set user values
                    if (password_verify($_POST['password'], $row['passwordHash'])) {
                        $_SESSION['userID'] = $row['userID'];

                        // Get role from userTypeID table and assign $role
                        $sql = 'SELECT userTypeID FROM userType WHERE userID = :userID';
                        $sth = $db->prepare($sql);
                        $sth->bindParam(':userID', $row['userID']);
                        $sth->execute();

                        // If role is fetched, redirect back to index.
                        if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                            $_SESSION['role'] = $row['userTypeID'];
                            header("Location: /");
                        } 
                        else {
                            echo "<script>\nwindow.parent.fail();\n</script>\n";
                        }
                    } 
                    else {
                        echo "<script>\nwindow.parent.fail();\n</script>\n";
                    }
                } 
                else {
                    echo "<script>\nwindow.parent.fail();\n</script>\n";
                }
            }
        }
    }
}