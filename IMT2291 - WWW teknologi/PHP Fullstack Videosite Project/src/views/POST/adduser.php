<?php

$addUserPage = new AddUser();
Router::addPostPath('/adduser', $addUserPage);

class AddUser extends Page {
    
    public function handle($args) {
        
        // Create and confirm DB connection
        $db = DB::getDBConnection();
        
        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        /*
        - Checks if a user exists in the database already.
        - Used before addUser function in order to prevent duplication.
        */
        function userExists($mail, $dbParam) {
            $sql = 'SELECT email FROM users WHERE email = :email';
            $sth = $dbParam->prepare($sql);
            $sth->bindParam(':email', $mail);
            $sth->execute();
            if($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                return $row['email'] == $mail;
            }
            return false;
        }

        // Check weather the user is logged in or not
        if(!User::isLoggedIn()) {

            // If a user submits the register form     
            if(isset($_POST['register'])) {

                // Checks if the user already exists in the database
                if(!userExists($_POST['email'], $db)) {
                    $sql = 'INSERT INTO users (fullName, email, passwordHash) 
                    VALUES (:fullName, :email, :passwordHash)';
                    $sth = $db->prepare($sql);
                    $sth->bindParam(':fullName', $_POST['fullName']);
                    $sth->bindParam(':email', $_POST['email']);
                    $sth->bindParam(':passwordHash', password_hash($_POST['password'], PASSWORD_DEFAULT));
                    $sth->execute();

                    if ($sth->rowCount() == 1) {
                        $temp['id'] = $db->lastInsertId();

                        // Add default permission role of 1 (student) to userTypeID table
                        $sql = 'INSERT INTO userType (userID, userTypeID) VALUES (:userID, :userTypeID)';
                        $sth = $db->prepare($sql);
                        $sth->bindParam(':userID', $temp['id']);
                        $sth->bindValue(':userTypeID', "1");
                        $sth->execute();

                        if ($sth->rowCount() == 1) {
                            echo "<script>\nwindow.parent.success();\n</script>\n";
                        }
                    } 
                    
                    else {
                        echo "<script>\nwindow.parent.fail();\n</script>\n";
                    }

                    // If checkbox for lecturer is set, insert into awaitingConfirmation
                    if (isset($_POST['confirmation'])) {
                        $sql = 'INSERT INTO awaitingConfirmation (userID) VALUES (:userID)';
                        $sth = $db->prepare($sql);
                        $sth->bindParam(':userID', $temp['id']);
                        $sth->execute();
                    }
                }
                else {
                    echo "<script>\nwindow.parent.fail();\n</script>\n";
                }
            }
        }
        else {
            header("Location: /");
        }
    }
}