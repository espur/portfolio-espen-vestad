<?php


$deletePage = new deleteVideo();


Router::addPostPath('/deleteVideo', $deletePage, "video.delete");



class deleteVideo extends Page
{
    public function handle($args)
    {
        $videoID = $_POST['videoID'];
        Video::deleteVideoByID($videoID);
        $filePath = "/var/www/html/videos/$videoID.mp4";
        unlink($filePath);
    }
}



