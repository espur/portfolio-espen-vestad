<?php


$editPage = new editVideo();

Router::addPostPath('/editVideo', $editPage, "video.edit");



class editVideo extends Page
{
    public function handle($args)
    {
        $results = Video::getVideoByID($_POST['videoID']);
        RouterPage::renderTwig('editVideo.html', array('queryResult' => $results));
    }
}