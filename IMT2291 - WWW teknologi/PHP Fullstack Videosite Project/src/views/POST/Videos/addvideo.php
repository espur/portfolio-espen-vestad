<?php


$addFilePage = new AddFile();

Router::addPostPath('/addfile', $addFilePage, "video.add");



class AddFile extends Page
{
    public function handle($args)
    {
        //check if file was uploaded
        if (is_uploaded_file($_FILES['videoToUpload']['tmp_name'])) {
                //addVideo adds video to database and returns ID if inserted or 0 if insert failed
            if ($id = Video::addVideo($_POST)) {
                if (@move_uploaded_file($_FILES['videoToUpload']['tmp_name'], "/var/www/html/videos/$id.mp4")) {
                    echo "<script>\nwindow.parent.success();\n</script>\n";
                } else {
                        //If the file is not stored delete database entry
                    Video::deleteVideoByID($id);
                    echo "<script>\nwindow.parent.fail();\n</script>\n";
                }
            } else {
                echo "<script>\nwindow.parent.fail();\n</script>\n";
            }
        } else {  // File not selected or some trickery going on
            echo "<script>\nwindow.parent.bad();\n</script>\n";
        }
    }
}