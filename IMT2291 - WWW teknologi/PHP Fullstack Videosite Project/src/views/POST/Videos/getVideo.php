<?php


$getVideo = new GetVideo();


Router::addPostPath("/getVideo", $getVideo, "video.get");


class GetVideo extends Page
{
    public function handle($args)
    {

        // Fetches videoID from hidden form, select current video from db
        // and redirects to /showVideo, which displays the video
        $db = DB::getDBConnection();
        $videoID = $_POST['videoID'];
        
        $sql = 'SELECT videoID FROM video WHERE videoID = :videoID';
        $sth = $db->prepare($sql);
        $sth->bindParam(':videoID', $videoID);
        $sth->execute();

        $video = $sth->fetch(PDO::FETCH_ASSOC);
        $_SESSION['videoID'] = $video['videoID'];
        
        header("Location: /showVideo");
        
    }
}