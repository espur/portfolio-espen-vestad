<?php


$addRating = new addRating();

Router::addPostPath('/addRating', $addRating, "video.rating.add");



class addRating extends Page
{
    public function handle($args)
    {
        Video::addRating($_POST['videoID'], $_POST['rating']);
    }
}