<?php


$addComment = new addComment();

Router::addPostPath('/addComment', $addComment, "video.comment.add");



class addComment extends Page
{
    public function handle($args)
    {
        Video::addComment($_POST['videoID'], $_POST['comment']);
    }
}