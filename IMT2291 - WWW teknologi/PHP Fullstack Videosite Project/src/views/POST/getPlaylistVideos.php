<?php


$getPlaylistVideos = new GetPlaylistVideos();

// Page for fetching all the videos in a certain playlist.
// Hidden POST form field with send the playlist ID to this handler.
Router::addPostPath("/getPlaylistVideos", $getPlaylistVideos, "playlist.videos.get");



class GetPlaylistVideos extends Page
{
    public function handle($args)
    {

        $db = DB::getDBConnection();
        
        // Retrieve playlist from database.
        $playlistID = $_POST['playlistID'];
        $playlist = Playlist::getPlayListByID($playlistID);
        
        // Retrieve playlist videoes from playlistPosition table
        // (please refer to UML diagram in repo wiki).
        $sql = 'SELECT * FROM playlistPosition WHERE playlistID = :playlistID';
        $sth = $db->prepare($sql);
        $sth->bindParam(':playlistID', $playlistID);
        $sth->execute();

        if($playPos = $sth->fetchAll(PDO::FETCH_ASSOC)) {
            
            $numOfRows = $sth->rowCount();

            // Grabs the videoes
            for($i = 0; $i < $numOfRows; $i++) {
                $sql = 'SELECT * FROM video WHERE videoID = :videoID';
                $sth = $db->prepare($sql);
                $sth->bindParam(':videoID', $playPos[$i]['videoID']);
                $sth->execute();

                if($videos[$i] = $sth->fetch(PDO::FETCH_ASSOC)) {
                    
                }
            }
            
            $_SESSION['playlistVideos'] = $videos;
            $_SESSION['playPos'] = $playPos;
                   
        }
        // Redirects to GET page.
        header("Location: /viewPlaylistVideos");  
    }
}