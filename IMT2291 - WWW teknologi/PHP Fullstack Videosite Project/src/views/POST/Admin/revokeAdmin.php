<?php

$revokeAdmin = new revokeAdmin();

Router::addPostPath('/admin/revoke', $revokeAdmin, "admin.revoke.admin");


class revokeAdmin extends Page
{
    public function handle($args)
    {
        User::revokeAdmin($_POST['userID']);
    }
}