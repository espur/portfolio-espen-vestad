<?php

$grantAdmin = new grantAdmin();

Router::addPostPath('/admin/grant', $grantAdmin, "admin.grant.admin");


class grantAdmin extends Page
{
    public function handle($args)
    {
        User::grantAdmin($_POST['userID']);
    }
}