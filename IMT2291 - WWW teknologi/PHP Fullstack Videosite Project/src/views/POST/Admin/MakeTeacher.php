<?php

$addTeacher = new AddTeacher();

Router::addPostPath('/admin/teacher', $addTeacher, "admin.make.teacher");


class AddTeacher extends Page
{
    public function handle($args)
    {
        $db = DB::getDBConnection();
        $userID = $_POST['userID'];

        if (User::grantTeacher($userID)) {

            $sql = "DELETE FROM awaitingConfirmation WHERE userID=:userID";
            $sth = $db->prepare($sql);
            $sth->bindParam(":userID", $userID);
            $sth->execute();
        }
    }
}