<?php


$editPlaylist = new editPlaylist();

Router::addPostPath('/editPlaylist', $editPlaylist, "playlist.edit");


class editPlaylist extends Page
{
    public function handle($args)
    {
        $playlistID = $_POST['playlistID'];
        $videoInPlaylist = Video::getVideosInPlaylist($playlistID);
        $videoNotInPlaylist = Video::getVideosNotInPlaylist($playlistID);
        $playlistResult = Playlist::getPlaylistByID($playlistID);

        RouterPage::renderTwig('editPlaylist.html', array('playlistResult' => $playlistResult, 'videoInPlaylist' => $videoInPlaylist, 'videoNotInPlaylist' => $videoNotInPlaylist));
    }
}
