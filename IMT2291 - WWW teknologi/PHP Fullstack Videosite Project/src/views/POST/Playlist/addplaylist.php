<?php


$addFilePage = new AddPlaylist();

Router::addPostPath('/addPlaylist', $addFilePage, "playlist.add");



class AddPlaylist extends Page
{
    public function handle($args)
    {
        Playlist::addPlaylist($_POST);
    }
}