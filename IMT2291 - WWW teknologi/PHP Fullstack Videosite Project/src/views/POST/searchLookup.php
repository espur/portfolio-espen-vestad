<?php


$searchLookup= new SearchLookup();

// Post path for searching videos or playlists. Retrieves data
// from hidden POST form in /search. Fetches matches with
// search terms and redirects to results page. Some of the
// search functions might be a bit broken. Please refer
// to the search part of our report in bitbucket wiki
Router::addPostPath('/searchLookup', $searchLookup, "user.search");


class SearchLookup extends Page
{
    public function handle($args)
    {
        
        $db = DB::getDBConnection();
        $videoResults;
        $playlistResults;
        $lecturerID = User::getUserIDByName($_POST['lecturer']);
        $_SESSION['search'] = 'OK';
        
        if(isset($_POST['searchVideos'])) {
            $videoResults = Video::searchForVideo($lecturerID['userID'], $_POST['subject'], $_POST['theme'], $_POST['title']);
            $_SESSION['searchedVideos'] = $videoResults;
        }
        if(isset($_POST['searchPlaylists'])) {
            $playlistResults = Playlist::searchForPlaylist($lecturerID['userID'], $_POST['title']);
            $_SESSION['searchedPlaylists'] = $playlistResults;
        }
        if(!isset($_POST['searchVideos']) && !isset($_POST['searchPlaylists'])) {
            $_SESSION['search'] = 'FAIL';
        }
        header("Location: /searchResults");
    }
}