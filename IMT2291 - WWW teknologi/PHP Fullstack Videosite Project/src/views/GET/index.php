<?php

// Custom Class
$page = new Index();

Router::addGetPath('/', $page);

class Index extends Page
{
    public function handle($args)
    {
        $db = DB::getDBConnection();

        if($db->NULL) {
            Router::error(404, "The DB could not connect.");
            die();
        }

        // Checks if user is logged in, and redirects to correct
        // user-page based on which role the user has.
        if(User::isLoggedIn()) {
            switch(User::whichRole()) {
                case 1: header("Location: /frontPage"); break;
                case 2: header("Location: /manageVideos"); break;
                case 3: header("Location: /admin/teacher"); break;
            }
        }   
        else {
            RouterPage::renderTwig('index.html', array());
        }

    }
}

