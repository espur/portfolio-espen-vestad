<?php


$logOut = new LogOut();

Router::addGetPath('/logOut', $logOut);


class LogOut extends Page
{
    public function handle($args)
    {
        // Unsets a user session and redirects back to logged-out page (aka index)
        session_unset();
        $_SESSION = array();
        header("Location: /");
    }
}