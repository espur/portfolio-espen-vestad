<?php

$reviewTeachers = new ReviewTeachers();

Router::addGetPath('/admin/teacher', $reviewTeachers, "admin.review.teacher");


class ReviewTeachers extends Page
{
    public function handle($args)
    {
        $db = DB::getDBConnection();

        $sql = "SELECT userID, fullName, email FROM users WHERE userID IN (select userID from awaitingConfirmation)";
        $sth = $db->prepare($sql);
        $sth->execute();
        $usersAwaitingConfirmation = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sql = "SELECT userID, fullName, email FROM users";
        $sth = $db->prepare($sql);
        $sth->execute();
        $allUsers = $sth->fetchAll(PDO::FETCH_ASSOC);

        RouterPage::renderTwig('admin/teacher.html', array('users' => $usersAwaitingConfirmation, 'allUsers' => $allUsers));
    }
}