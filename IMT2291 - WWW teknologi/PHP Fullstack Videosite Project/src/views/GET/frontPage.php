<?php


$frontPage = new FrontPage();

Router::addGetPath('/frontPage', $frontPage, "frontpage.view");


class FrontPage extends Page
{
    public function handle($args)
    {

        $db = DB::getDBConnection();


        $subscriptions = array();
        $playlists = array();
        $lecturerName = array();
        $userID = User::getUserID();
        $userRole = User::whichRole();
        $playlistStatus = "Empty";
        $subStatus = "None";

        $sql = 'SELECT subscription, playlistID FROM subscription WHERE userID = :userID';
        $sth = $db->prepare($sql);
        $sth->bindParam(':userID', $userID);
        $sth->execute();

        if ($subscriptions = $sth->fetch(PDO::FETCH_ASSOC)) {
            $subStatus = 'Set';
        }

        $sql = 'SELECT * FROM playlist WHERE playlistID = :playlistID';
        $sth = $db->prepare($sql);
        $sth->bindParam(':playlistID', $subscriptions['playlistID']);
        $sth->execute();

        if ($playlists = $sth->fetchAll(PDO::FETCH_ASSOC)) {
            $playlistStatus = 'Set';

        }
        RouterPage::renderTwig('frontPage.html', array('subStatus' => $subStatus, 'playlistStatus' => $playlistStatus, 'playlists' => $playlists, 'role' => $userRole));

    }
}