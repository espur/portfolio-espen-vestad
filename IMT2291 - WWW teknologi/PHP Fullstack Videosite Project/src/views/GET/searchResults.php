<?php


$searchResults = new SearchResults();

Router::addGetPath('/searchResults', $searchResults, "user.search.results");

class SearchResults extends Page
{
    public function handle($args)
    {
        // Display playlist and video results from search
        $playlistRes = $_SESSION['searchedPlaylists'];
        $videosRes = $_SESSION['searchedVideos'];
        
        RouterPage::RenderTwig("searchResults.html", array('playlists' => $playlistRes, 'videos' => $videosRes, 'status' => $_SESSION['search']));

    }
}