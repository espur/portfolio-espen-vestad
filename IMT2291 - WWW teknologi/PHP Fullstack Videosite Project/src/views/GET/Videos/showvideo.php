<?php


$showVideo = new showVideo();

Router::addGetPath('/showVideo', $showVideo, "video.show");


class showVideo extends Page
{
    public function handle($args)
    {   
        
        $video = Video::getVideoByID($_SESSION['videoID']);
        RouterPage::renderTwig('showVideo.html', array('videoResults' => $video));
    }
}
