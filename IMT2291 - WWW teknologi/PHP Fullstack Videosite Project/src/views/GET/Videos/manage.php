<?php


$managePage = new manageVideos();

Router::addGetPath('/manageVideos', $managePage, "videos.manage");


class manageVideos extends Page
{
    public function handle($args)
    {
        $videoResult = Video::getUserVideos();
        $playlistResult = Playlist::getUserPlaylist();
        
        RouterPage::renderTwig('manageVideos.html', array('videoResult' => $videoResult, 'playlistResult' => $playlistResult));
    }
}
