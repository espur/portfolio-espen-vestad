<?php


$managePage = new newPlaylist();

Router::addGetPath('/newPlaylist', $managePage, "playlist.create");


class newPlaylist extends Page
{
    public function handle($args)
    {
        $results = Video::getUserVideos();
        
        RouterPage::renderTwig('newPlaylist.html', array('queryResult' => $results));
    }
}