<?php

// Page for viewing videos in a subscribed playlist. Gets playlist information
// from $_SESSION, which retrieves it's values from the getPlaylistVideos.php file.

$viewPlaylistVideos = new ViewPlaylistVideos();


Router::addGetPath("/viewPlaylistVideos", $viewPlaylistVideos, "playlist.video.view");


class ViewPlaylistVideos extends Page
{
    public function handle($args)
    {

        
        $videos = $_SESSION['playlistVideos'];
        $playPos = $_SESSION['playPos'];

        RouterPage::renderTwig('viewPlaylistVideos.html', array('videos' => $videos, 'position' => $playPos));


    }
}