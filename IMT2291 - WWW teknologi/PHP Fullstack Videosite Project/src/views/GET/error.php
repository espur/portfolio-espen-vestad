<?php
// Custom Class
$page = new ErrorPage();

// Slap that into the router
Router::addGetPath('error', $page);

class ErrorPage extends Page
{
    public function handle($args)
    {
        RouterPage::renderTwig('error.html', ["errorMessage"=>$args]);
    }
}


