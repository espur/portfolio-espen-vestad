CREATE TABLE `awaitingConfirmation`
(
    `awaitingConfirmationID` int(11) NOT NULL,
    `userID`                 int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;

CREATE TABLE `permission`
(
    `permissionID`   int(11) NOT NULL,
    `permissionName` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `permissionUserType`
(
    `userTypeID`           int(11) NOT NULL,
    `permissionID`         int(11) NOT NULL,
    `permissionUserTypeID` int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `playlist`
(
    `playlistID`   int(11) NOT NULL,
    `lecturer`     int(11)                       DEFAULT NULL,
    `title`        varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `description`  varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `thumbnailUrl` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `playlistPosition`
(
    `playlistPositionID` int(11) NOT NULL,
    `videoID`            int(11) DEFAULT NULL,
    `playlistID`         int(11) DEFAULT NULL,
    `position`           int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;

CREATE TABLE `ratings`
(
    `ratingsID` int(11) NOT NULL,
    `userID`    int(11) DEFAULT NULL,
    `videoID`   int(11) DEFAULT NULL,
    `score`     int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `subscription`
(
    `subscription` int(11) NOT NULL,
    `playlistID`   int(11) DEFAULT NULL,
    `userID`       int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `users`
(
    `userID`       int(11) NOT NULL,
    `email`        varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `fullName`     varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `passwordHash` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `userType`
(
    `userID`     int(11) NOT NULL,
    `userTypeID` int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `video`
(
    `videoID`      int(11) NOT NULL,
    `title`        varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `description`  varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `thumbnailUrl` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `subject`      varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `theme`        varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `lecturer`     int(11)                       DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


CREATE TABLE `videoComment`
(
    `commentId` int(11) NOT NULL,
    `userID`    int(11)                       DEFAULT NULL,
    `videoID`   int(11)                       DEFAULT NULL,
    `comment`   varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


ALTER TABLE `awaitingConfirmation`
    ADD PRIMARY KEY (`awaitingConfirmationID`),
    ADD KEY `FK_user_confirmation` (`userID`);


ALTER TABLE `permission`
    ADD PRIMARY KEY (`permissionID`),
    ADD UNIQUE KEY `permissionName` (`permissionName`);


ALTER TABLE `permissionUserType`
    ADD PRIMARY KEY (`permissionUserTypeID`),

    ADD KEY `Fk_permission_ID` (`permissionID`);


ALTER TABLE `playlist`
    ADD PRIMARY KEY (`playlistID`),
    ADD KEY `FK_playlist_lecturer` (`lecturer`);


ALTER TABLE `playlistPosition`
    ADD PRIMARY KEY (`playlistPositionID`),
    ADD KEY `FK_playlistPos_videoID` (`videoID`);


ALTER TABLE `ratings`
    ADD PRIMARY KEY (`ratingsID`),
    ADD KEY `FK_ratings_userID` (`userID`),
    ADD KEY `FK_ratings_videoID` (`videoID`);


ALTER TABLE `subscription`
    ADD PRIMARY KEY (`subscription`),
    ADD KEY `FK_subscription_playlistID` (`playlistID`),
    ADD KEY `userID` (`userID`);


ALTER TABLE `users`
    ADD PRIMARY KEY (`userID`);


ALTER TABLE `userType`
    ADD PRIMARY KEY (`userID`);


ALTER TABLE `video`
    ADD PRIMARY KEY (`videoID`),
    ADD KEY `FK_video_lecturer` (`lecturer`);


ALTER TABLE `videoComment`
    ADD PRIMARY KEY (`commentId`),
    ADD KEY `FK_comment_userID` (`userID`),
    ADD KEY `FK_comment_videoID` (`videoID`);


ALTER TABLE `awaitingConfirmation`
    MODIFY `awaitingConfirmationID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `permission`
    MODIFY `permissionID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `playlist`
    MODIFY `playlistID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `playlistPosition`
    MODIFY `playlistPositionID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `ratings`
    MODIFY `ratingsID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `subscription`
    MODIFY `subscription` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `users`
    MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `video`
    MODIFY `videoID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `videoComment`
    MODIFY `commentId` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `permissionUserType`
    MODIFY `permissionUserTypeID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `awaitingConfirmation`
    ADD CONSTRAINT `FK_user_confirmation` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `playlist`
    ADD CONSTRAINT `FK_playlist_lecturer` FOREIGN KEY (`lecturer`) REFERENCES `users` (`userID`) ON DELETE SET NULL ON UPDATE CASCADE;


ALTER TABLE `playlistPosition`
    ADD CONSTRAINT `FK_playlistPos_videoID` FOREIGN KEY (`videoID`) REFERENCES `video` (`videoID`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `FK_playlistPos_playlist_ID` FOREIGN KEY (`playlistID`) REFERENCES `playlist` (`playlistID`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `ratings`
    ADD CONSTRAINT `FK_ratings_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `FK_ratings_videoID` FOREIGN KEY (`videoID`) REFERENCES `video` (`videoID`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `subscription`
    ADD CONSTRAINT `FK_subscription_playlistID` FOREIGN KEY (`playlistID`) REFERENCES `playlist` (`playlistID`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE SET NULL ON UPDATE CASCADE;


ALTER TABLE `userType`
    ADD CONSTRAINT `FK_user_userType` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `video`
    ADD CONSTRAINT `FK_video_lecturer` FOREIGN KEY (`lecturer`) REFERENCES `users` (`userID`) ON DELETE SET NULL ON UPDATE SET NULL;


ALTER TABLE `videoComment`
    ADD CONSTRAINT `FK_comment_userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `FK_comment_videoID` FOREIGN KEY (`videoID`) REFERENCES `video` (`videoID`) ON DELETE CASCADE ON UPDATE CASCADE;


INSERT INTO `users` (`userID`, `email`, `fullName`, `passwordHash`)
VALUES (1, 'bruker1@bruker.no', 'Bruker1', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (2, 'bruker2@bruker.no', 'Bruker2', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (3, 'bruker3@bruker.no', 'Bruker3', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (4, 'bruker4@bruker.no', 'Bruker4', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (5, 'bruker5@bruker.no', 'Bruker5', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (7, 'Lecturer1@Lecturer.no', 'Lecturer1', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (8, 'Lecturer2@Lecturer.no', 'Lecturer2', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (9, 'Lecturer3@Lecturer.no', 'Lecturer3', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (10, 'Lecturer4@Lecturer.no', 'Lecturer4', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (11, 'Lecturer5@Lecturer.no', 'Lecturer5', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi'),
       (12, 'admin@admin.no', 'admin', '$2y$10$7j9rzidkzC7nNYyqqDQAo.62Ar/TJdJVPICWlxc4G4UzaMrfHazXi');

INSERT INTO `userType` (`userID`, `userTypeID`)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (7, 1),
       (8, 2),
       (9, 2),
       (10, 1),
       (11, 1),
       (12, 3);

INSERT INTO `awaitingConfirmation` (`awaitingConfirmationID`, `userID`)
VALUES (1, 7),
       (2, 10),
       (3, 11);

INSERT INTO `video` (`title`, `description`, `thumbnailUrl`, `subject`, `theme`, `lecturer`)
VALUES ('title', 'Dette er en description', 'https://i.imgur.com/lwUu1eu.jpg', 'PHP', 'Data', '1'),
       ('title', 'Om it', 'https://i.imgur.com/lwUu1eu.jpg', 'CSS', 'Data', '2'),
       ('stein', 'Dette Geir', 'https://i.imgur.com/lwUu1eu.jpg', 'Stein', 'Geologi', '1'),
       ('Steinkule greier', 'Om steiner', 'https://i.imgur.com/lwUu1eu.jpg', 'Golf', 'Sport', '3'),
       ('Ruting', 'Dette er en description', 'https://i.imgur.com/lwUu1eu.jpg', 'PHP', 'IT', '4');


INSERT INTO `playlist` (`playlistID`, `lecturer`, `title`, `description`, `thumbnailUrl`)
VALUES (1, 1, 'Min spilleliste', 'Dette er en spilleliste', 'https://i.imgur.com/lwUu1eu.jpg');


INSERT INTO `playlistPosition` (`playlistPositionID`, `videoID`, `playlistID`, `position`)
VALUES (1, 3, 1, 0),
       (2, 1, 1, 1);

INSERT INTO `subscription` (`playlistID`, `userID`)
VALUES (1, 1);

INSERT INTO `permission` (`permissionName`)
VALUES ('admin.make.teacher'),
       ('admin.review.teacher'),
       ('frontpage.view'),
       ('playlist.add'),
       ('playlist.create'),
       ('playlist.delete'),
       ('playlist.edit'),
       ('playlist.update'),
       ('playlist.video.view'),
       ('user.search'),
       ('user.search.results'),
       ('video.add'),
       ('video.comment.add'),
       ('video.delete'),
       ('video.edit'),
       ('video.get'),
       ('video.rating.add'),
       ('video.show'),
       ('video.update'),
       ('video.upload'),
       ('videos.manage');

INSERT INTO `permissionUserType` (`userTypeID`, `permissionID`)
VALUES ('3', '1'),
       ('3', '2'),
       ('1', '4'),
       ('2', '5'),
       ('2', '6'),
       ('2', '7'),
       ('2', '8'),
       ('2', '9'),
       ('1', '10'),
       ('1', '11'),
       ('1', '12'),
       ('2', '13'),
       ('1', '14'),
       ('2', '15'),
       ('2', '16'),
       ('1', '17'),
       ('1', '18'),
       ('1', '19'),
       ('2', '20'),
       ('2', '21');